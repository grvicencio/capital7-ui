<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/','DashboardController@index');
Route::get('/dashboard', 'DashboardController@index')->name('defaultpage');
Route::get('/account/profile', 'DashboardController@profileView');
Route::get('/account/details', 'DashboardController@accountDetailsView');
Route::get('/account/wallet', 'DashboardController@walletView');
Route::get('/account/creditcardregistration', 'DashboardController@creditcardregistrationView');
Route::get('/transaction/redeemcodes', 'DashboardController@reedemCodesView');
Route::get('/transaction/history', 'DashboardController@transactionHistoryView');
Route::get('/account/security', 'DashboardController@securityView');
Route::get('/account/notification', 'DashboardController@notificationView');
Route::post('LoginProcess', 'ProfileController@LoginProcess')->name('login');
Route::get('mydashboard', 'DashboardController@userdashboard')->name('userdashboard');
Route::get('openapplication/{application}','ProfileController@ApplicationOpen')->name('open.application');

Route::get('/generateSSOToken{application}', 'UserController@generateSSOToken');

Route::get('logout', 'UserController@logout');
Route::post('user/registration', 'UserController@Registerprocess');

Route::get('activate/{code}', 'UserController@activateAccount');