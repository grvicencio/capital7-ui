<?php

namespace App\Mail;

use App\CRM\baccarat\Accounts;
use App\CRM\NinepineModels\EmailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CrmDepositEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $account;
    protected $amount;
    protected $notes;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Accounts $account, $amount, $notes)
    {
        $this->account = $account;
        $this->amount = $amount;
        $this->notes = $notes;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email_template = EmailTemplate::where("email_type", "CRM_DEPOSIT")->first();

        $content = render($email_template->content, [
            'name' => $this->account->getDisplayName(),
            'amount' => $this->amount,
            'notes' => $this->notes,
            'app_url' => config("app.url") . '/admin/login',
            'app_name' => config("app.name")
        ]);

        return $this->view("layouts.email", compact('content'))
            ->subject($email_template->subject)
            ->from(config('mail.from.address'), config('mail.from.name'))
            ->to(config('mail.from.deposit.address'), config('mail.from.deposit.name'));
    }
}
