<?php

namespace App\Mail;

use App\CRM\baccarat\Accounts;
use App\CRM\NinepineModels\EmailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class WebTransferFromCrmEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $receiver;
    protected $amount;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Accounts $receiver, $amount)
    {
        $this->receiver = $receiver;
        $this->amount = $amount;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email_template = EmailTemplate::where("email_type", "WEB_TRANSFER_FROM_CRM")->first();

        $content = render($email_template->content, [
            'receiver' => $this->receiver->getDisplayName(),
            'amount' => $this->amount,
            'app_url' => config("app.url"),
            'app_name' => config("app.name")
        ]);

        return $this->view("layouts.email", compact('content'))
            ->subject($email_template->subject)
            ->from(config('mail.from.transfer.address'), config('mail.from.transfer.name'))
            ->to($this->receiver->email, $this->receiver->getDisplayName());
    }
}
