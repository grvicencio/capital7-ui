<?php

namespace App\Mail;

use App\CRM\NinepineModels\EmailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WebForgotPasswordEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $account;
    protected $link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($account, $link)
    {
        $this->account = $account;
        $this->link = $link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email_template = EmailTemplate::where("email_type", "WEB_FORGOT_PASSWORD")->first();

        $content = render($email_template->content, [
            'name' => $this->account->getDisplayName(),
            'link' => $this->link,
            'app_name' => config("app.name")
        ]);

        return $this->view("layouts.email", compact('content'))
            ->subject($email_template->subject)
            ->from(config('mail.from.address'), config('mail.from.name'))
            ->to($this->account->email, $this->account->getDisplayName());
    }
}
