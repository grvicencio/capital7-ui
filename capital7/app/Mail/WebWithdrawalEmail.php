<?php

namespace App\Mail;

use App\CRM\baccarat\Accounts;
use App\CRM\NinepineModels\EmailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WebWithdrawalEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $account;
    protected $amount;
    protected $notes;
    protected $status;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Accounts $account, $amount, $notes, $status)
    {
        $this->account = $account;
        $this->amount = $amount;
        $this->notes = $notes;
        $this->status = $status;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email_template = EmailTemplate::where("email_type", "WEB_WITHDRAWAL")->first();

        $content = render($email_template->content, [
            'name' => $this->account->getDisplayName(),
            'amount' => $this->amount,
            'notes' => $this->notes,
            'status' => $this->status,
            'app_url' => config("app.url"),
            'app_name' => config("app.name")
        ]);

        return $this->view("layouts.email", compact('content'))
            ->subject($email_template->subject)
            ->from(config('mail.from.withdrawal.address'), config('mail.from.withdrawal.name'))
            ->to($this->account->email, $this->account->getDisplayName());
    }
}
