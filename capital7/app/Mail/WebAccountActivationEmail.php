<?php

namespace App\Mail;


use App\CRM\NinepineModels\EmailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Blade;

class WebAccountActivationEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;
    protected $activation_code;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data, $activation_code)
    {
        $this->data = $data;
        $this->activation_code = $activation_code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */


    public function build()
    {

        $link = config('app.url') . "/activate/" . $this->activation_code;
//        $link = 'http://127.0.0.1:8000' . "/activate/" . $this->activation_code;
        $content1 = "Hello {$this->data['email']},

                    Please click the provided button below to activate your account.
                    
                    {$link}
                    
                    Note: This is auto generated email system. Do not Reply.
                    
                    Thank you,
                    
                    Pine Team";

                    $php = Blade::compileString($content1);

                    $obLevel = ob_get_level();
                    ob_start();
                    extract([
                        'name' =>  $this->data['email'],
                        'link' => $link,
                        'app_name' => config("app.name")
                    ], EXTR_SKIP);

                    try {
                        eval('?' . '>' . $php);
                    } catch (Exception $e) {
                        while (ob_get_level() > $obLevel) ob_end_clean();
                        throw $e;
                    } catch (Throwable $e) {
                        while (ob_get_level() > $obLevel) ob_end_clean();
                        throw new FatalThrowableError($e);
                    }

                    $content =  ob_get_clean();

        return $this->view("mail.email", compact('content'))
            ->subject("Account Activation Link")
            ->from(config('mail.from.address'), config('mail.from.name'))
            ->to($this->data['email'], $this->data['email']);
    }
}
