<?php

namespace App\Mail;

use App\common\TradeMarketTicket;
use App\CRM\NinepineModels\EmailTemplate;
use App\CRM\NinepineModels\Wallet;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WebTicketHasPaymentEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $ticket;
    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(TradeMarketTicket $ticket, $data = [])
    {
        $this->ticket = $ticket;
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email_template = EmailTemplate::where("email_type", "WEB_TICKET_HAS_PAYMENT")->first();

        $content = render($email_template->content, [
            'trader' => $this->ticket->trader_account->getDisplayName(),
            'offer' => $this->ticket->inquiry_account->getDisplayName(),
            'offer_type' => $this->ticket->getType(),
            'offer_ticket_generated_id' => $this->ticket->ticket_generated_id,
            'offer_value' => $this->ticket->currency->currency . ' ' .  number_format($this->ticket->value, 2),
            'offer_amount' => $this->ticket->parent->trade_profile->crypto_currency->currency . ' ' .  number_format($this->ticket->amount, 8),
            'notes' => $this->ticket->trade_market_ticket_deposit()->first()->offer_notes,
            'deposit_id' => $this->ticket->trade_market_ticket_deposit()->first()->id,
            'ticket_generated_id' => $this->ticket->parent->ticket_generated_id,
            'app_url' => config("app.url"),
            'app_name' => config("app.name")
        ]);

        return $this->view("layouts.email", compact('content'))
            ->subject($email_template->subject)
            ->from(config('mail.from.address'), config('mail.from.name'))
            ->to($this->ticket->trader_account->email, $this->ticket->trader_account->getDisplayName());
    }
}
