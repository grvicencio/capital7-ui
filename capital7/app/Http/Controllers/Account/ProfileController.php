<?php
/**
 * Created by PhpStorm.
 * User: NPT 8
 * Date: 8/13/2018
 * Time: 11:34 AM
 */

namespace App\Http\Controllers\Account;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Ixudra\Curl\Facades\Curl;

class ProfileController extends Controller
{

    public function profileView(){


        if (empty(Session::get('access_token'))) {
            return redirect()->route('defaultpage');
        }
        $data['sidebar'] = true;
        $data['title'] = 'Account';
        $data['sub_title'] = 'Profile';
        $data['account_menu'] = true;
        $data['profile_menu'] = true;


        $data['sidebar'] = true;
        $data['title'] = 'Dashboard';
        $data['sub_title'] = 'Home';
        $data['islogin'] = false;
        $data['accountdetails_menu'] = true;
        $bearer = "Bearer ". Session::get('access_token');
        $profile = Curl::to(config('api.url') . config('api.endpoints.user.profile'))
            ->withContentType('application/json')
            ->withHeader('Authorization:' . $bearer)
            ->withHeader('Accept: application/json')
            ->returnResponseObject()
            ->get();
        $profile =  $profile->content;
        $profile = json_decode($profile, true);
        if(!$profile) {
            return redirect('logout');
        }

        if  (array_key_exists('error', $profile)) {
            return redirect('logout');
        }
        $data['profile'] = $profile;

        return view('account/profile')->with($data);
    }
    public function accountDetailsView(){


        if (empty(Session::get('access_token'))) {
            return redirect()->route('defaultpage');
        }
        $data['sidebar'] = true;
        $data['title'] = 'Account';
        $data['sub_title'] = 'Profile';
        $data['account_menu'] = true;
        $data['sidebar'] = true;
        $data['title'] = 'Dashboard';
        $data['sub_title'] = 'Home';
        $data['islogin'] = false;
        $data['accountdetails_menu'] = true;
        $bearer = "Bearer ". Session::get('access_token');
        $profile = Curl::to(config('api.url') . config('api.endpoints.user.profile'))
            ->withContentType('application/json')
            ->withHeader('Authorization:' . $bearer)
            ->withHeader('Accept: application/json')
            ->returnResponseObject()
            ->get();
        $profile =  $profile->content;
        $profile = json_decode($profile, true);
        if(!$profile) {
            return redirect('logout');
        }

        if  (array_key_exists('error', $profile)) {
            return redirect('logout');
        }
        $data['profile'] = $profile;

        return view('account/profile')->with($data);
    }
    public function notificationView(){


        if (empty(Session::get('access_token'))) {
            return redirect()->route('defaultpage');
        }
        $data['sidebar'] = true;
        $data['title'] = 'Account';
        $data['sub_title'] = 'Profile';
        $data['account_menu'] = true;
        $data['profile_menu'] = true;


        $data['sidebar'] = true;
        $data['title'] = 'Dashboard';
        $data['sub_title'] = 'Home';
        $data['islogin'] = false;
        $data['notification_menu'] = true;
        $bearer = "Bearer ". Session::get('access_token');
        $profile = Curl::to(config('api.url') . config('api.endpoints.user.profile'))
            ->withContentType('application/json')
            ->withHeader('Authorization:' . $bearer)
            ->withHeader('Accept: application/json')
            ->returnResponseObject()
            ->get();
        $profile =  $profile->content;
        $profile = json_decode($profile, true);
        if(!$profile) {
            return redirect('logout');
        }

        if  (array_key_exists('error', $profile)) {
            return redirect('logout');
        }
        $data['profile'] = $profile;

        return view('account/profile')->with($data);
    }




}