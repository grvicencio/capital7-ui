<?php

namespace App\Http\Controllers;

use App\API\Currency;
use App\CRM\NinepineModels\ExchangeRate;
use App\Http\Controllers\Controller;
use App\WEB\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Ixudra\Curl\Facades\Curl;
use App\WEB\Category;
use App\Http\Controllers\WEB\Wallet\UserWallet;

class DashboardController extends Controller {

	public function index(){
		$data['sidebar'] = false;
		$data['title'] = 'Dashboard';
		$data['sub_title'] = 'Home';
		$data['islogin'] = false;
		$data['dashboard_menu'] = true;
        $bearer = "Bearer ". Session::get('access_token');
        $profile = Curl::to(config('api.url') . config('api.endpoints.user.profile'))
            ->withContentType('application/json')
            ->withHeader('Authorization:' . $bearer)
            ->withHeader('Accept: application/json')
            ->returnResponseObject()
            ->get();
        $profile =  $profile->content;
        $profile = json_decode($profile, true);

        if(!$profile) {
            return redirect('logout');
        }

        if  (array_key_exists('error', $profile)) {
            return redirect('logout');
        }

        $data['profile'] = $profile;

		return view('dashboard/dashboard')
			->with($data);
	}
	public function userdashboard() {
		if (empty(Session::get('access_token'))) {
			return redirect()->route('defaultpage');
		}

        $data['sidebar'] = false;
        $data['title'] = 'Dashboard';
        $data['sub_title'] = 'Home';
        $data['islogin'] = false;
        $data['dashboard_menu'] = true;
        $bearer = "Bearer ". Session::get('access_token');
        $profile = Curl::to(config('api.url') . config('api.endpoints.user.profile'))
            ->withContentType('application/json')
            ->withHeader('Authorization:' . $bearer)
            ->withHeader('Accept: application/json')
            ->returnResponseObject()
            ->get();
        $profile =  $profile->content;
        $profile = json_decode($profile, true);
        if(!$profile) {
            return redirect('logout');
        }

        if  (array_key_exists('error', $profile)) {
            return redirect('logout');
        }
        $data['profile'] = $profile;
        return view('dashboard/dashboard')->with($data);
	}

    public function profileView() {

        if (empty(Session::get('access_token'))) {
            return redirect()->route('defaultpage');
        }
        $data['sidebar'] = true;
        $data['title'] = 'Account';
        $data['sub_title'] = 'Profile';
        $data['account_menu'] = true;
        $data['profile_menu'] = true;
        $data['displayBackButton'] = true;


        $data['sidebar'] = true;
        $data['title'] = 'Dashboard';
        $data['sub_title'] = 'Home';
        $data['islogin'] = false;
        $data['accountdetails_menu'] = true;
        $bearer = "Bearer ". Session::get('access_token');
        $profile = Curl::to(config('api.url') . config('api.endpoints.user.profile'))
            ->withContentType('application/json')
            ->withHeader('Authorization:' . $bearer)
            ->withHeader('Accept: application/json')
            ->returnResponseObject()
            ->get();
        $profile =  $profile->content;
        $profile = json_decode($profile, true);
        if(!$profile) {
            return redirect('logout');
        }

        if  (array_key_exists('error', $profile)) {
            return redirect('logout');
        }
        $data['profile'] = $profile;
        return view('account/profile')->with($data);
    }
    public function accountDetailsView() {

        if (empty(Session::get('access_token'))) {
            return redirect()->route('defaultpage');
        }
        $data['sidebar'] = true;
        $data['title'] = 'Account';
        $data['sub_title'] = 'Profile';
        $data['displayBackButton'] = true;


        $data['sidebar'] = true;
        $data['title'] = 'Dashboard';
        $data['sub_title'] = 'Home';
        $data['islogin'] = false;
        $data['accdet_menu'] = true;
        $bearer = "Bearer ". Session::get('access_token');
        $profile = Curl::to(config('api.url') . config('api.endpoints.user.profile'))
            ->withContentType('application/json')
            ->withHeader('Authorization:' . $bearer)
            ->withHeader('Accept: application/json')
            ->returnResponseObject()
            ->get();
        $profile =  $profile->content;
        $profile = json_decode($profile, true);
        if(!$profile) {
            return redirect('logout');
        }

        if  (array_key_exists('error', $profile)) {
            return redirect('logout');
        }
        $data['profile'] = $profile;
        return view('account/detail')->with($data);
    }
    public function walletView() {

        if (empty(Session::get('access_token'))) {
            return redirect()->route('defaultpage');
        }
        $data['sidebar'] = true;
        $data['title'] = 'Account';
        $data['sub_title'] = 'Profile';
        $data['displayBackButton'] = true;



        $data['sidebar'] = true;
        $data['title'] = 'Dashboard';
        $data['sub_title'] = 'Home';
        $data['islogin'] = false;
        $data['wallet_menu'] = true;
        $bearer = "Bearer ". Session::get('access_token');
        $profile = Curl::to(config('api.url') . config('api.endpoints.user.profile'))
            ->withContentType('application/json')
            ->withHeader('Authorization:' . $bearer)
            ->withHeader('Accept: application/json')
            ->returnResponseObject()
            ->get();
        $profile =  $profile->content;
        $profile = json_decode($profile, true);
        if(!$profile) {
            return redirect('logout');
        }

        if  (array_key_exists('error', $profile)) {
            return redirect('logout');
        }
        $data['profile'] = $profile;
        return view('account/wallet')->with($data);
    }
    public function creditcardregistrationView() {

        if (empty(Session::get('access_token'))) {
            return redirect()->route('defaultpage');
        }
        $data['sidebar'] = true;
        $data['title'] = 'Account';
        $data['sub_title'] = 'Profile';
        $data['displayBackButton'] = true;
        $data['sidebar'] = true;
        $data['title'] = 'Dashboard';
        $data['sub_title'] = 'Home';
        $data['islogin'] = false;
        $data['wallet_menu'] = true;
        $bearer = "Bearer ". Session::get('access_token');
        $profile = Curl::to(config('api.url') . config('api.endpoints.user.profile'))
            ->withContentType('application/json')
            ->withHeader('Authorization:' . $bearer)
            ->withHeader('Accept: application/json')
            ->returnResponseObject()
            ->get();
        $profile =  $profile->content;
        $profile = json_decode($profile, true);
        if(!$profile) {
            return redirect('logout');
        }
        if  (array_key_exists('error', $profile)) {
            return redirect('logout');
        }
        $data['profile'] = $profile;
        return view('account/card-registration')->with($data);
    }
    public function reedemCodesView() {

        if (empty(Session::get('access_token'))) {
            return redirect()->route('defaultpage');
        }
        $data['sidebar'] = true;
        $data['title'] = 'Account';
        $data['sub_title'] = 'Profile';

        $data['displayBackButton'] = true;


        $data['sidebar'] = true;
        $data['title'] = 'Dashboard';
        $data['sub_title'] = 'Home';
        $data['islogin'] = false;
        $data['redeem_menu'] = true;
        $bearer = "Bearer ". Session::get('access_token');
        $profile = Curl::to(config('api.url') . config('api.endpoints.user.profile'))
            ->withContentType('application/json')
            ->withHeader('Authorization:' . $bearer)
            ->withHeader('Accept: application/json')
            ->returnResponseObject()
            ->get();
        $profile =  $profile->content;
        $profile = json_decode($profile, true);
        if(!$profile) {
            return redirect('logout');
        }

        if  (array_key_exists('error', $profile)) {
            return redirect('logout');
        }
        $data['profile'] = $profile;
        return view('account/reedem')->with($data);
    }
    public function transactionHistoryView() {

        if (empty(Session::get('access_token'))) {
            return redirect()->route('defaultpage');
        }
        $data['sidebar'] = true;
        $data['title'] = 'Account';
        $data['sub_title'] = 'Profile';
        $data['displayBackButton'] = true;



        $data['sidebar'] = true;
        $data['title'] = 'Dashboard';
        $data['sub_title'] = 'Home';
        $data['islogin'] = false;
        $data['history_menu'] = true;
        $bearer = "Bearer ". Session::get('access_token');
        $profile = Curl::to(config('api.url') . config('api.endpoints.user.profile'))
            ->withContentType('application/json')
            ->withHeader('Authorization:' . $bearer)
            ->withHeader('Accept: application/json')
            ->returnResponseObject()
            ->get();
        $profile =  $profile->content;
        $profile = json_decode($profile, true);
        if(!$profile) {
            return redirect('logout');
        }

        if  (array_key_exists('error', $profile)) {
            return redirect('logout');
        }
        $data['profile'] = $profile;
        return view('account/transactionHistory')->with($data);
    }
    public function securityView() {

        if (empty(Session::get('access_token'))) {
            return redirect()->route('defaultpage');
        }
        $data['sidebar'] = true;
        $data['title'] = 'Account';
        $data['sub_title'] = 'Profile';
        $data['displayBackButton'] = true;



        $data['sidebar'] = true;
        $data['title'] = 'Dashboard';
        $data['sub_title'] = 'Home';
        $data['islogin'] = false;
        $data['security_menu'] = true;
        $bearer = "Bearer ". Session::get('access_token');
        $profile = Curl::to(config('api.url') . config('api.endpoints.user.profile'))
            ->withContentType('application/json')
            ->withHeader('Authorization:' . $bearer)
            ->withHeader('Accept: application/json')
            ->returnResponseObject()
            ->get();
        $profile =  $profile->content;
        $profile = json_decode($profile, true);
        if(!$profile) {
            return redirect('logout');
        }

        if  (array_key_exists('error', $profile)) {
            return redirect('logout');
        }
        $data['profile'] = $profile;
            return view('account/security')->with($data);
        }
    public function notificationView() {

        if (empty(Session::get('access_token'))) {
            return redirect()->route('defaultpage');
        }
        $data['sidebar'] = true;
        $data['title'] = 'Account';
        $data['sub_title'] = 'Profile';
        $data['account_menu'] = true;
        $data['sidebar'] = true;
        $data['title'] = 'Dashboard';
        $data['sub_title'] = 'Home';
        $data['islogin'] = false;
        $data['notification_menu'] = true;
        $data['displayBackButton'] = true;

        $bearer = "Bearer ". Session::get('access_token');
        $profile = Curl::to(config('api.url') . config('api.endpoints.user.profile'))
            ->withContentType('application/json')
            ->withHeader('Authorization:' . $bearer)
            ->withHeader('Accept: application/json')
            ->returnResponseObject()
            ->get();
        $profile =  $profile->content;
        $profile = json_decode($profile, true);
        if(!$profile) {
            return redirect('logout');
        }

        if  (array_key_exists('error', $profile)) {
            return redirect('logout');
        }
        $data['profile'] = $profile;
        return view('account/notification')->with($data);
    }

}