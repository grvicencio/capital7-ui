<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;
use App\Http\Requests\LoginProcess;
use Session;
use Validator;
use Illuminate\Support\Facades\Redirect;
class ProfileController extends Controller
{
    //
    public function LoginProcess(Request $request)
    {
    	
    	$username  = $request->username;
    	$password = $request->password;
   

    	$data = array('username' =>$username, 'password' =>$password);

    	$response = Curl::to(config('api.url') . config('api.endpoints.user.login'))
						->withData($data )
						->returnResponseObject()
						->post();

        $r =  json_decode($response->content,true);
       
       if (isset($r['access_token'])) {
       		Session::put('access_token', $r['access_token']);
	        Session::put('refresh_token', $r['refresh_token']);
	        $response = array('success'=>'true');
	        
	         return response()->json($response,200);
       } else {
       		
       			$response =array('success'=>'false', 'msg' =>$r['error']); 
       		return response()->json($response,200);
       }
        

    }

    public function ApplicationOpen($application) {
    	
    	$url = config('api.url') . 'ReadySSO/'.$application;
    	$bearer = "Bearer ". Session::get('access_token');
    	switch($application) {
    		   case "Capital7" : 
    		   	$destination = config('ssourl.Capital7') . "ValidatePineSSO?token=%s&application=Capital7";
    		   	break;
    		   case "Baccarat":
                   $destination = config('ssourl.Baccarat') . "ValidatePineSSO?token=%s&application=Baccarat";
    		   	break;
    		   	case "RakeBackAsia":
    		   		$destination = config('ssourl.RakeBackAsia') . "ValidatePineSSO?token=%s&application=RakeBackAsia";
    		   		
    		   	break;	
    	}
    	$response = Curl::to($url)
						->withContentType('application/json')
						->withHeader('Authorization:' . $bearer)
						->withHeader('Accept: application/json')
						->returnResponseObject()
						->get();
				
		$r = json_decode($response->content,true);				
   if (isset($r['error'])) {
    $response = array('Result' =>'You have been sign out');
 
    return Redirect::back();

   }
		if ($r['result'] =='success') {
			$token = $r['data'];
			
				$urltoopen= sprintf($destination, $token);
				return Redirect::to($urltoopen);
		} else {
			return  json_encode($response,true);
		}			


    }
}
