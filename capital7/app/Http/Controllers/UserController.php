<?php

namespace App\Http\Controllers;


use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Ixudra\Curl\Facades\Curl;
use App\Http\Requests\UserRegister;
use App\Http\Requests\LoginProcess;
use App\Http\Requests\UpdateProfile;
use Redirect;
use Session;
use Validator;
use App\Mail\WebRegisterEmail;
use App\Mail\WebAccountActivationEmail;
use App\Jobs\SendRegistrationEmail;
class UserController extends Controller
{
    public function Registerprocess(Request $request) {
		$email = $request->email;
		$password = $request->password;
		$repeat_password = $request->repeat_password;
		$first_name= $request->first_name;
		$last_name= $request->last_name;
        $data = array('email' =>$email, 'password' =>$password, 'repeat_password' =>$repeat_password, 'first_name' =>$first_name, 'last_name' =>$last_name);

        $response = Curl::to(Config('api.url') . Config('api.endpoints.user.register'))
            ->withHeader('Accept: application/json')
            ->withData( $data )
            ->returnResponseObject()
            ->post();
        $result = $response->content;
        \Log::debug($result);
        $result = json_decode($result);
        if(property_exists($result, 'error')) {
             $message = '';
            if (property_exists($result->error, 'password')) {
                $message = $result->error->password[0];
            } elseif (property_exists($result->error, 'repeat_password')) {
                $message = $result->error->repeat_password[0];
            } elseif (property_exists($result->error, 'email')) {
                $message = $result->error->email[0];
            } elseif (property_exists($result->error, 'birthdate')) {
                $message = $result->error->birthdate[0];
            } else {
                $message = "Error in creating account!";
            }
            $response = array();
            $response[0] = "error";
            $response[1] = $message;
            return $response ;
        }

        if($result == "success"){
            $response = array();
            $response[0] = $result->result;
            $response[1] = "success";

        }else
        {
            $response = array();
            $response[0] = $result->result;
            $response[1] = "Success";
            $activation_code = $result->data->activation_code;
            $body_data = $data;

//            $job = (new SendRegistrationEmail($body_data,$activation_code))->delay(Carbon::now()->addMinutes(1))->onConnection('database');
//            dispatch($job);
            if($activation_code != 'next') {
                $template ='mail.email';
                $job = (new SendRegistrationEmail($body_data,$activation_code))->delay(Carbon::now()->addMinutes(1))->onConnection('database');
                   dispatch($job);
            }
        }
            return $response ;
    }
    public function LoginProcess(LoginProcess $request)
    {
    	$username  = $request->username;
    	$password = $request->password;

    	$data = array('username' =>$username, 'password' =>$password);
    	$response = Curl::to(Config('apiconfig.api_url') . Config('apiconfig.endpoints.user.login'))
						->withData($data )
						->returnResponseObject()
						->post();
	     $r =  json_decode($response->content,true);
	     if  (array_key_exists('error', $r)) {
             foreach($r as $k => $v) {
                 $errors[$k] = $v;
             }


             $errors = array_reverse($errors, true);

             return Redirect::back()->withErrors($errors);
         }

        Session::put('access_token', $r['access_token']);
	     Session::put('refresh_token', $r['refresh_token']);
	     return Redirect('/dashboard');

    }
    public function LoginMobile(Request $request)
    {

        $username  = $request->username;
        $password = $request->password;
        $data = array('username' =>$username, 'password' =>$password);
        $response = Curl::to(Config('apiconfig.api_url') . Config('apiconfig.endpoints.user.login'))
            ->withData($data)
            ->returnResponseObject()
            ->post();

        $r =  json_decode($response->content,true);
        $msg = "";
        if  (array_key_exists('error', $r)) {
            $request->session()->flash('message', 'Invalid username/email or password');
            return redirect()->back();
//            return Redirect::back()->withErrors($errors);
        }else{

            Session::put('access_token', $r['access_token']);
            Session::put('refresh_token', $r['refresh_token']);
            return Redirect('account/card-registration');
        }


    }
    public function Logout() {
        $bearer = "Bearer ". Session::get('access_token');

        $response = Curl::to(Config('apiconfig.api_url') . Config('apiconfig.endpoints.user.logout'))
            ->withContentType('application/json')
            ->withHeader('Authorization:' . $bearer)
            ->withHeader('Accept: application/json')
            ->returnResponseObject()
            ->post();
        $owner =  $response->content;
        $owner = json_decode($owner, true);
        $data['islogin'] = true;
        $data['sidebar'] = false;
        Session::put('access_token', '');
        return view('dashboard/dashboard')->with($data);
    }
    public function sendContactUsEmail(ContactUsEmailProcess $request){
        $content = $request->message;
        $recipient = config('email.crmadmin');
        $isTest = $request->isTest;
        $g_recaptcha_response = $request->input('g-recaptcha-response');



        $verify=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . env('GOOGLE_SECRET_KEY') . "&response=$g_recaptcha_response");
        if(isset($isTest))
            $verify=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" .'6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe'. "&response=$g_recaptcha_response");

        $captcha_success=json_decode($verify);
        if ($captcha_success->success==false) {
          //This user was not verified by recaptcha.
            return (['error', 'Something is wrong with your captcha field! Answering Captcha is required']);

        }
        else if ($captcha_success->success==true) {
            $data = array(
                'subject'=> $request->subject,
                'type'=> $request->type,
                'title' => $request->subject,
                'user' => $request->name,
                'content' => $content,
                'recipient' => $recipient,
                'sender' => $request->email
            );

            $template ='email.contactUs';

            \Mail::send($template, $data, function ($message) use ($data)
            {
                $message->from(Config('email.contactUs'),'');
                $message->subject($data['subject']);
                $message->to($data['recipient']);
            });

            return (['success', 'Thank you for contacting our support team! We\'ll get back to you as soon as we can.'] );

        }




    }
    public function UpdateProfile(UpdateProfile $request) {
        $username = $request->username;
        $email = $request->email;
        $bearer = "Bearer ". Session::get('access_token');
        $data = array('username' =>$username, 'email' =>$email);

        $response = Curl::to(Config('apiconfig.api_url') . Config('apiconfig.endpoints.user.update'))

            ->withHeader('Authorization:' . $bearer)
            ->withHeader('Accept: application/json')
            ->withData($data )
            ->returnResponseObject()
            ->post();

		$result = json_decode($response->content, true);

		if  (array_key_exists('error', $result)) {
			
    	 	$errors = array();
	     	$error = $result['error'];
	     	foreach($error as $k =>$v) {
	     		
	     		$errors[$k] = $v[0];
	     	}
	     	return Redirect::back()->withErrors($errors);

			
		} else {
			return Redirect::back()->withErrors($result);
		}				
		

    }
    public function addCard(Request $request)
    {


        $card_no = $request->input('card_no');
        $card_name = $request->input('card_name');
        $card_cvc = $request->input('cvc');
        $card_expiry = $request->input('valid_thru');
        $body_data = [
            'card_no' => $card_no,
            'card_name' => $card_name,
            'cvc' => $card_cvc,
            'valid_thru' => $card_expiry,
        ];

        $bearer = "Bearer ". Session::get('access_token');
        $response = Curl::to(Config('apiconfig.api_url') . Config('apiconfig.endpoints.payment.create'))
            ->withHeader('Authorization:' . $bearer)
            ->withHeader('Accept: application/json')
            ->withData( $body_data )
            ->returnResponseObject()
            ->post();



        $result = $response->content;
        $result= json_decode($result, true);

        if($result['result']=="success"){
            $request->session()->flash('message1', $result['result'] . " - " . $result['message']);
          return $this->saveBillingInfo($request);
        }else{
            $request->session()->flash('message1', 'Unable to save card info please check your details!');
            return redirect('account/card-registration')->withInput();
        }





    }
    public function saveBillingInfo(Request $request){

        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        $city = $request->input('city');
        $billing_address = $request->input('billing_address');
        $billing_address_second = $request->input('billing_address_second');
        $postalcode = $request->input('postalcode');
        $phone = $request->input('phone');
        $country = $request->input('country');
        $body_data = [
            'first_name' => $first_name,
            'last_name' => $last_name,
            'city' => $city,
            'billing_address' => $billing_address,
            'billing_address_second' => $billing_address_second,
            'postalcode' => $postalcode,
            'phone' => $phone,
            'country' => $country
        ];

        $bearer = "Bearer ". Session::get('access_token');
        $response = Curl::to(Config('apiconfig.api_url') . Config('apiconfig.endpoints.user.billing_save'))
            ->withHeader('Authorization:' . $bearer)
            ->withHeader('Accept: application/json')
            ->withData($body_data)
            ->returnResponseObject()
            ->post();


        $result = $response->content;
        $result= json_decode($result, true);

        $request->session()->flash('message2', $result['message']);
        return redirect('account/card-registration')->withInput();

    }
    public function activateAccount(Request $request, $activationCode){
        $response = Curl::to(Config('apiconfig.api_url') . Config('apiconfig.endpoints.user.activate') . $activationCode)
            ->withContentType('application/json')
            ->withHeader('Accept: application/json')
            ->returnResponseObject()
            ->get();
        $response =  $response->content;
        $response = json_decode($response, true);
        $data['messageAlertResult'] = $response['result'];
        $data['messageAlertMessage'] = $response['message'];
        $bearer = "Bearer ". Session::get('access_token');

        $response = Curl::to(Config('apiconfig.api_url') . Config('apiconfig.endpoints.user.logout'))
            ->withContentType('application/json')
            ->withHeader('Authorization:' . $bearer)
            ->withHeader('Accept: application/json')
            ->returnResponseObject()
            ->post();
        $owner =  $response->content;
        $owner = json_decode($owner, true);
        $data['islogin'] = true;
        $data['sidebar'] = false;
        Session::put('access_token', '');
        return view('dashboard/dashboard')->with($data);
    }
}
