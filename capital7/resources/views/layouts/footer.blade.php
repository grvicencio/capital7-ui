
<footer class="row main-footer top-content" style="background-color: #222222; height: 50px;">
	<div class="pull-right" style="color:white; margin-top:5px; margin-right: 4%;">
		<strong>Copyright &copy; {{ date('Y') }} <a href="http://ninepinetech.com"><span style="color:#baac4a;">CAPITAL 7</span></a>.</strong>
		{{--All rights reserved. Version 1.0 | <a href="{{ route('web.contact') }}">Contact Us</a>--}}
		All rights reserved.
	</div>
</footer>