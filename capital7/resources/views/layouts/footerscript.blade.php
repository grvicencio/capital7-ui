<!-- jQuery 2.2.3 -->
<script src="{{ asset('bower_components/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>

<!-- Bootstrap 3.3.6 -->
<script src="{{ asset ("bower_components/AdminLTE/bootstrap/js/bootstrap.min.js") }}"></script>
<!-- AdminLTE App -->
{{--<script src="{{ asset ("WEB/bower_components/AdminLTE/dist/js/app.min.js") }}"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.2/js/adminlte.js"></script>
<!-- SlimScroll -->
<script src="{{ asset ("bower_components/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js") }}"></script>

<script src="{{ asset ("js/WEB/sweetalert2.all.js") }}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
<script src="{{ asset("CRM/AdminLTE-2.4.2/bower_components/select2/dist/js/select2.full.min.js") }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.11/dist/sweetalert2.all.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        @isset($profile_image_file)
            $(".profile-image").attr("src", "data:image/png;base64,{{ $profile_image_file }}");
        @endisset
    });

    $(window).scroll( function() {
        $('#sidebar').height(($('#div_holder').height()) + "px");
    });

    $('#form_user_registration').on('submit', function(e){
        e.preventDefault();
        var form = $('#form_user_registration');
        var btn = form.find(':submit').button('loading');
        btn.button('loading');
        $('button').attr('disabled',true);
        $.ajax({
            url: "/account/register",
            type: "post",
            data: $('#form_user_registration').serialize(),
            success: function(data) {
                $('button').attr('disabled',false);
                if(data[0] == 'success'){
                    $('#form_user_registration').trigger('reset');
                    $(".close").click();
                    swal({
                        type: 'success',
                        title: 'Success!',
                        text: data[1],
                    })
                }else
                    $('#div_msg').html('' +
                        '' +
                        '<div class="alert alert-warning alert-dismissible show" role="alert">\n' +
                        '  <strong>Error!</strong> '+  data[1] +'.\n' +
                        '  <button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
                        '    <span aria-hidden="true">&times;</span>\n' +
                        '  </button>\n' +
                        '</div>' +
                        '' +
                        '' +
                        '');
                btn.button('reset');
                $('button').attr('disabled', false);

                $(".alert").fadeTo(2000, 500).slideUp('slow', function(){
                    $(".alert").alert('close');
                });
            },
            error: function(xhr){

                var ErrorArr = $.map(xhr.responseJSON, function(el) { return el; });
                var error = "";
                ErrorArr.forEach(function(element) {
                    error = element + "<br>";
                });
                $('#div_msg').html('' +
                    '' +
                    '<div class="alert alert-warning alert-dismissible show" role="alert">\n' +
                    '  <strong>Error! </strong> '+  error + '\n' +
                    '  <button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
                    '    <span aria-hidden="true">&times;</span>\n' +
                    '  </button>\n' +
                    '</div>' +
                    '' +
                    '' +
                    '');
                $('button').attr('disabled',false);
                btn.button('reset');
                $(".alert").fadeTo(2000, 500).slideUp('slow', function(){
                    $(".alert").alert('close');
                });
            }
        });
    });

    function changeAttachment(input){
        var url = input.value;
        var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();

        if(input.files && input.files[0] && (ext == "png" || ext == "jpeg" || ext == "jpg")){
            if((input.files[0].size / 1024) <= 2000){
            } else{
                swal(
                    'Error!',
                    'Maximum image size is 2MB.',
                    'error'
                );
                input.value = "";
            }
        } else{
            swal(
                'Error!',
                'Invalid image format. Only png, jpg, jpeg  are accepted as an attachment.',
                'error'
            );
            input.value = "";
        }
    }
</script>