<!DOCTYPE html>

<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    @include('WEB.layouts.header')
    @yield('header_styles')
    @yield('header_scripts')
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.standalone.css">

    <style>

        form {
            overflow: visible;
        }

        .bootstrap-datetimepicker-widget {
            overflow: visible;
        }

        #register-card {
            padding: 0 0 0;
            margin: auto;
            float: none;
        }

        .message {
            background-color: #14805e;
            border-color: #14805e;
            font-weight: 500;
            color: #FFF;
            box-shadow: none;
            padding: 10px;
        }

        .btn {
            border-radius: 0px;

        }

        form {
            margin-left: 0;
            padding: 26px 26px 2px;
            font-weight: 400;
            overflow: visible;
            background: #fff;
        }

        hr {
            border-top: 1px dashed #8c8b8b;
            border-bottom: 1px dashed #fff;
        }

        input.btn.btn-primary {
            border-radius: 0px;
        }

        .form-control {
            border-radius: 0px;
        }

        .strike {
            display: block;
            text-align: center;
            overflow: hidden;
            white-space: nowrap;
            font-size: 12px;
        }

        .strike > span {
            position: relative;
            display: inline-block;
        }

        .strike > span:before,
        .strike > span:after {
            content: "-";
            position: absolute;
            top: 50%;
            width: 9999px;
            /* Here is the modification */
            border-top: 1px dotted #8c8b8b;
        }

        .strike > span:before {
            right: 100%;

        }

        .strike > span:after {
            left: 100%;
            margin-left: 5px;
            margin-bottom: 5px;
        }

        hr {
            border-top: 1px dotted #8c8b8b;
        }

        strong {
            font-size: 15px;
        }

        span.input-group-addon:hover {
            cursor: pointer;
        }

        hr {
            border-top: 1px dashed #ffffff;
            border-bottom: 1px dashed #ffffff;
        }

        p.message {
            text-transform: uppercase;
            font-weight: bold;
            text-align: center;
            color: white;
        }

        textarea {
            resize: none;
        }

        #register-card {
            background: white;
            padding: 0 0 0;
            margin: auto;
            float: none;

        }

        @media (max-width: 767px) {

            #register-card {
                padding: 0 0 0;
                margin: auto;
                float: none;

            }

            .container-fluid {
                padding: 0px;
            }

            form#form_login {
                height: -webkit-fill-available;
            }

            label {
                letter-spacing: 1px;
            }
        }

        .message {
            padding-bottom: 0px;
        }

        form {
            padding-top: 15px;
        }

        hr {
            margin: 0px;
            border: 0;

        }



        .bg-yellow, .callout.callout-warning, .alert-warning, .label-warning, .modal-warning .modal-body {
            background-color: #ffe7a0 !important;
            border-color: #ffe7a0 !important;
        }

        body {
            background-image: url("{{ url('/images/background.jpg') }}");
            background-repeat: no-repeat;
            background-: no-repeat;

        }

        .container-fluid {
            background: rgba(0, 0, 0, 0.3);
            height: -webkit-fill-available;
        }

        #register-card {
            margin-top: 2%;
            box-shadow: 0px 0px 5px 5px #84808099;
        }

        .message {
            background-color: #ffffff;
            border-color: #14805e;
            color: black;
        }

        .g-recaptcha.pull-right {
            padding-bottom: 10px;
        }

    </style>
    <script src='https://www.google.com/recaptcha/api.js' async defer></script>

</head>
<body>
<div id="background_holder">
</div>
<div class="container-fluid">


    <div id="register-card" class="col-xs-12 col-sm-5 col-md-3">
        <hr>
        <div class="message text-center">
            <img class="img-responsive pull-left" src="{{ url('/images/mail.png') }}"
                 style="width:100px; height:100px;">
            <h3> We are happy to hear anything from you.</h3>

        </div>
        <br>

        <form name="form_contact_us" id="form_contact_us" action="/sendContactUsEmail" method="post">
            <div id="div_msg" class='form-group'>


            </div>
            {{ csrf_field() }}





            <div class="row strike">
                <span> &nbsp;  <i class="fa fa-paper-plane"></i>   <strong>Contact Us</strong> </span>
            </div>
            <div class="form-group">
                <label for=""><i class="fa fa-info-circle"></i></i> Type:</label>
                <select id="type" name="type" class="form-control"  value="{{old('type')}}" required>
                    <option value="Inquiry">Inquiry</option>
                    <option value="Issue">Issue</option>

                </select>
            </div>
            <div class="form-group">
                <label for=""><i class="fa fa-info-circle"></i> Subject:</label>
                <input type="text" name="subject" id="subject" class="form-control" value="{{old('subject')}}" required>
            </div>
            <div class="form-group">
                <label for=""><i class="fa fa-info-circle"></i> Name:</label>
                <input type="text" name="name" id="name" class="form-control" value="{{old('name')}}" required>
            </div>

            <div class="form-group">
                <label for=""><i class="fa fa-envelope"></i> Email:</label>
                <input type="email" name="email" id="email" class="form-control" value="{{old('email')}}" required>
            </div>
            <div class="form-group">
                <label for=""><i class="fa fa-comment"></i> Message:</label>
                <textarea name="message" id="message" class="form-control" required rows="7">{{old('message')}}</textarea>
            </div>
            <div class="form-group">
                <div id="g_recaptcha" class="g-recaptcha pull-right" data-callback="capcha_filled"
                     data-expired-callback="capcha_expired" data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}"></div>
            </div>

            <br>
            <div class="row">
                <div class="col-sm-12">
                    <br>
                    <button id="btn_submit" class="btn btn-success pull-right" type="submit" data-loading-text="Please wait..."><i
                                class="fa fa-paper-plane"></i> Send
                    </button>
                </div>
            </div>

        </form>

        <br>

    </div>

</div>

@include('WEB.layouts.footerscript')
@yield('footer_scripts')

<script>

    
    $('#form_contact_us').on('submit', function(e){
        e.preventDefault();
        var form = $(this);
        var btn = form.find(':submit').button('loading');
            btn.button('loading');
        $('#btn_submit').prop('disabled', true);
        $.ajax({
            url: "/sendContactUsEmail",

            type: "post",

            data: $("#form_contact_us").serialize(),

            success: function(data) {
                $('#div_msg').html('   <div class="row">\n' +
                    '                        <div id = "alert_box" class="alert">\n' +
                    '                            <div class="box-body">\n' +
                                                    data[1] +
                    '                            </div>\n' +
                    '                        </div>\n' +
                    '                    </div>');


                $('#btn_submit').prop('disabled', false);
                btn.button('reset');
                grecaptcha.reset();
                var css_class = "alert-success";
                if(data[0] == "error"){
                    css_class = "alert-danger";
                }else
                    $('#form_contact_us').trigger("reset");
                $('#alert_box').addClass(css_class);
            }

        });



    });

</script>

</body>
</html>