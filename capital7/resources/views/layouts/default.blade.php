<!DOCTYPE html>
<html>
<head>
    {{-- ================================================== FOR UI MODIFICATION PURPOSES; COMMENT LINE; ================================================== --}}
    {{-- RELOAD PAGE EVERY 5 SECONDS --}}
    {{-- <meta http-equiv="refresh" content="10; URL=http://bitcoin.ninepine/"> --}}
    {{-- ================================================== FOR UI MODIFICATION PURPOSES; COMMENT LINE; ================================================== --}}

    @include('layouts.header')

    @yield('header_styles')
    @yield('header_scripts')

    <link rel="stylesheet" href="https://unpkg.com/nprogress@0.2.0/nprogress.css">
    <style>
        #nprogress .bar {
            background: #DDBF20;
        }

        footer.main-footer {
            margin-left: 0;
            bottom: 0 !important;
            margin-bottom: 0;
            position: fixed;
        }

        .swal2-popup {
            font-size: 1.5rem !important;
        }

        .chat-unavailable {
            background: #DDDDDD !important;
            opacity: 0.3;
        }
    </style>

</head>
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">
    <!-- Modal -->

    @if($islogin ==true)
    <div class="modal fade" id="modal_login" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <img class="header-img" src="{{  asset("images/WEB/CAPITAL 7  LOGO.png") }}">
                    <strong class="header-label">Capital7</strong>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">X</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p style="font-family: 'gotham-light'; font-size:15px;"><strong> Access All your account in our<br>
                            Website services, all with one account.</strong>
                    </p>
                    <a href="#" style="color:#21549e; font-style: italic;"> Learn More</a>
                    <br>
                    <br>
                    <form name="formlogin"  id="formlogin" method="post" action="">

                        <div class="form-group">
                          <div class="alert alert-danger" id="errorlogin"style="display:none">

</div>
                          {{ csrf_field() }}
                            <input type="email" class="form-control modal-input-text" id="username" aria-describedby="emailHelp" placeholder="Sign-In ID(Email Address)" name="username">
                            <input type="password" class="form-control modal-input-text" id="password" name="password" placeholder="Password">
                            <br>

                        </div>
                        <button type="submit" id="btn_login" class="btn btn-modal btn-block btn-primary">Submit</button>
                        <br>
                        <a href="#" style="color:#21549e; font-style: italic; font-family: 'gotham-book'"> Trouble Signing in?</a>
                        <br>
                        <br>
                        <div style="font-family: 'gotham-book'" class="text-center">Terms/Privacy/Help </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_registration" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <img class="header-img" src="{{ asset("images/WEB/CAPITAL 7  LOGO.png")}}">
                    <strong class="header-label">Capital7</strong>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">X</span>
                    </button>
                </div>
                <div class="modal-body">
                   <form id="form-register" action="" method="post">
                       {{ csrf_field() }}
                       <div class="form-group form-inline">
                           <input type="text" style="width: 55%;" class="form-control modal-input-text" id="first_name" name="first_name"  placeholder="First name" required>
                           <input type="text" style="width: 41%; margin-left: 2%" class="form-control modal-input-text" name="last_name" id="last_name" placeholder="Last name" required>
                       </div>
                       <div class="form-group">
                           <input type="email"  class="form-control modal-input-text" id="register-email" name="email"  placeholder="Email address" required>
                       </div>
                        <div class="form-group">
                           <input type="password"  class="form-control modal-input-text" id="register-password" name="password"  placeholder="Password" required>
                       </div>
                       <div class="form-group">
                           <input type="password"  class="form-control modal-input-text" id="repeat_password" name="repeat_password"  placeholder="Confirm password" required>
                       </div>
                       {{--<div class="form-group form-inline">--}}
                           {{--<input type="text" style="width: 20%;" class="form-control modal-input-text" id="mobilecountrycode"  placeholder="+63">--}}
                           {{--<input type="text" style="width: 76%; margin-left: 2%" class="form-control modal-input-text" id="mobilenumber" placeholder="Mobile phone number">--}}
                       {{--</div>--}}
                       {{--<div class="form-group form-inline">--}}
                           {{--<select id="birthmonth" style="width: 49%;" class="form-control modal-input-text" required>--}}
                           {{--</select>--}}
                           {{--<input type="text" style="width: 24%;" class="form-control modal-input-text" id="birthday"  placeholder="Day">--}}
                           {{--<input type="text" style="width: 24%;" class="form-control modal-input-text" id="birthyear" placeholder="Year">--}}
                       {{--</div>--}}
                       {{--<div class="form-group">--}}
                           {{--<select id="sign-up-gender" class="form-control modal-input-text" required>--}}
                               {{--<option value="" disabled selected hidden>Gender (optional)</option>--}}
                               {{--<option value="Male">Male</option>--}}
                               {{--<option value="Female" >Female</option>--}}
                           {{--</select>--}}
                       {{--</div>--}}
                       <p style="font-family: 'gotham-book'; font-size:10px;">
                           By clicking "Continue", you agree to the <a class="links" href="#">Terms (Updated) </a> and <a class="links" href="#">Privacy Policy (Updated) </a>
                       </p>
                        <br>
                       <button type="submit" id="btn_signup" class="btn btn-modal btn-block btn-primary">Continue</button>
                       <br>
                       <div class="text-center">
                           <p style="font-family: 'gotham-book'; font-size:13px;">
                               Already have an account? <a class="links" href="#">Sign in</a>
                           </p>
                       </div>
                   </form>
                </div>

            </div>
        </div>
    </div>
   @else
        <div class="modal fade" id="modal_add_application" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <img class="header-img" src="{{asset("images/WEB/Capital7 LOGO.png")}}">
                        <strong class="header-label">Capital7</strong>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">X</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="panel modal-cards">
                            <div class="panel-body text-center">
                                <img class="app-img" src="{{ asset("images/WEB/TIPS.png") }}">
                                <br>
                                <div align="left">
                                    <b class="app-title"> Tips </b><br>
                                    <p class="app-developer"> Ninepine Tech </p>
                                    <p class="app-description "> Tourism Mobile apps help users for golf tour, planning travel, accommodation bookings, ticket bookings, cab booking, and more. </p>
                                </div>

                                <button type="submit" style="width:80%;" class="btn btn-modal btn-primary btn-add-application">Add Application</button>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    @endif
    @include('layouts.top_nav')

        <div class="div-content">
            @if($sidebar)
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-2  sidebar-holder">
                    @include('layouts.sidebar')
                </div>
                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-10 ">
                    @yield('content')
                </div>
                @else
                @yield('content')
            @endif

        </div>
</div>


@include('layouts.footerscript')
@include('layouts.footer')
@yield('footer_scripts')

<script src="https://unpkg.com/nprogress@0.2.0/nprogress.js" type="text/javascript"></script>
<script>

    function setModalMaxHeight(element) {
        this.$element     = $(element);
        this.$content     = this.$element.find('.modal-content');
        var borderWidth   = this.$content.outerHeight() - this.$content.innerHeight();
        var dialogMargin  = $(window).width() < 768 ? 20 : 60;
        var contentHeight = $(window).height() - (dialogMargin + borderWidth);
        var headerHeight  = this.$element.find('.modal-header').outerHeight() || 0;
        var footerHeight  = this.$element.find('.modal-footer').outerHeight() || 0;
        var maxHeight     = contentHeight - (headerHeight + footerHeight);

        this.$content.css({
            'overflow': 'hidden'
        });

        this.$element
            .find('.modal-body').css({
            'max-height': maxHeight,
            'overflow-y': 'auto'
        });
    }

    $('.modal').on('show.bs.modal', function(e) {
        $(e.relatedTarget).addClass("active");
        $(this).show();
        setModalMaxHeight(this);
    });
   $('.modal').on('hide.bs.modal', function(e) {
       $('a.btn.btn-user-action.btn-primary.active').removeClass("active");
       $(e.relatedTarget).removeClass("active");
    });

    $(window).resize(function() {
        if ($('.modal.in').length != 0) {
            setModalMaxHeight($('.modal.in'));
        }
    });
    const monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];

    for(var x=1; x<=12; x++){
        $('#birthmonth').append($('<option>', {
            value: x,
            text: monthNames[x-1]
        }));
    }
    $('#form-register').on('submit',function(e){
        e.preventDefault();

        if($('#register-password').val() != $('#repeat_password').val()){
            swal({
                title: 'Error!',
                text: "Password and confirm password didn't match",
                type: 'error',
                confirmButtonText: 'OK'
            });
        }else{
            $.ajax({
                url: "user/registration",
                type: "post",
                data: $('form').serialize(),
                success: function(data) {
                    console.log(data);
                    if(data[0].toLowerCase()=="success")
                    swal({
                        title: 'Success!',
                        text: "",
                        type: 'success',
                        confirmButtonText: 'OK'
                    }).then(function(result){
                        if (result.value) {
                            window.location.reload();
                    }
                });
                    else{
                        swal({
                            title: data[0].toUpperCase(),
                            text: data[1],
                            type: 'error',
                            confirmButtonText: 'OK'
                        });
                    }
                }
            });
        }


    });

</script>

<script>
   $(function() {

      $('#btn_login').click(function(e){
        e.preventDefault();
       // console.log($('#formlogin').serialize());
        $.ajax({
          url: "{{route('login')}}",
          method:"post",
          dataType: "html",
          data: $('#formlogin').serialize(),
          success: function(result) {
            console.log(result);
            var d =  jQuery.parseJSON(result);
             var success = d.success;
             if (success=='false') {
                //if (d.msg =='invalid_credentials') {
                    $( "#errorlogin" ).html('Invalid Username / Password');
                    $("#errorlogin").show();

             } else {
              window.location.href = "{{route('userdashboard')}}";
             }

           // console.log(result);
          }

        });
        return false;
      });
   })
</script>
{{-- /CHAT --}}
</body>
</html>