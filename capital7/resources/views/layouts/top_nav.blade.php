<header class="main-header">
    <nav class="navbar navbar-static-top">
        <div class="container-fluid">
            <div class="row top-content" style="color: #291609; ">
                <div class="top-content-text pull-right">Capital7</div>
            </div>
            <div class="navbar-header">
                {{--<a href="/" class="navbar-brand"><img src="{{ asset("images/WEB/logo.png") }}" class="img-responsive" style="height:50px; background-color: white; padding:10px;">  </a>--}}
                <a href="/" class="navbar-brand"> <img src="{{ asset("images/WEB/logocapitalseven.png") }}" style="    width: 70px;
    height: 65px;
    margin-left: 122px;"></a>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#navbar-collapse" aria-expanded="false">
                    <i class="fa fa-bars"></i>
                </button>
            </div>

            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav">

                    @if(isset($displayBackButton))
                    <li class="" ><a href="/">Back To Apps</a></li>
                        @else

                    @endif
                </ul>
                <ul class="nav navbar-nav pull-right user-info-holder">
                    @if($islogin==true)
                    <li class="@isset($account_menu) active @endisset"><a class="btn btn-user-action btn-primary" id="btn_login_header" style="margin-right: -15px !important;" href="{{ config('app.API_URL')}}">Sign in</a></li>
                    @else
                    <li class="dropdown hidden-xs">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="/images/WEB/4B.png" class="user-image"
                                 alt="User Image">
                            <span class="hidden-xs">
										{{ isset($profile['username']) ?  $profile['username'] : "" }}
										&nbsp; <i class="fa fa-caret-down"></i>
									</span>
                        </a>

                        <ul class="dropdown-menu">
                            <li><a href="/account/details">Account Settings</a></li>
                            <li><a href="/account/wallet">Payment Info</a></li>
                            <li><a href="/transaction/redeemcodes">Reedem Codes</a></li>
                            <li><a href="/logout">Sign Out</a></li>
                        </ul>
                    </li>
                        <li class="dropdown hidden-sm hidden-md hidden-lg pull-right ">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="/images/WEB/4B.png" class="user-image"
                                     alt="User Image">
                                <span >
										{{$profile['username']}}
										&nbsp; <i class="fa fa-caret-down"></i>
									</span>
                            </a>

                            <ul class="dropdown-menu">
                                <li><a href="/account/profile">Account Settings</a></li>
                                <li><a href="/account/security">Payment Info</a></li>
                                <li><a href="/logout">Reedem Codes</a></li>
                                <li><a href="/logout">Sign Out</a></li>
                            </ul>
                        </li>
                        @endif
                </ul>
            </div>

        </div>
    </nav>
</header>

{{--@if(!isset($profile['id']))--}}
{{--@include('layouts.registration')--}}
{{--@endif--}}