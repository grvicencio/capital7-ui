
<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModal3Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
                    <span aria-hidden="true"><i class="fa fa-times-circle" aria-hidden="true"></i></span>
                </button>
                <h4 class="modal-title" id="exampleModal3Label"><i class="fa fa-user" aria-hidden="true"></i> Create your own account!</h4>
            </div>
            <div class="modal-body">
                <form name="form_user_registration" id="form_user_registration" action="/account/register" method="post">
                    <div  id="div_msg" class="row">

                    </div>

                    {{ csrf_field() }}
                    <hr>
                    <div class="form-group">
                        <label for="inputName">Last Name</label>
                        <input type="text" class="form-control" id="lastname" aria-describedby="nameHelp" placeholder="Enter name" name="lastname" required>
                        <small id="inputName" class="form-text text-muted">Your lastname.</small>
                    </div>
                    <div class="form-group">
                        <label for="inputName">First Name</label>
                        <input type="text" class="form-control" id="firstName" aria-describedby="nameHelp" placeholder="Enter name" name="firstname" required>
                        <small id="inputName" class="form-text text-muted">Your firstname.</small>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Username</label>
                        <input type="text" class="form-control" id="userName" aria-describedby="nameHelp" placeholder="Enter name" name="username" required>
                        <small id="inputName" class="form-text text-muted">Must be unique.</small>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name="email" required>
                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" id="password" placeholder="Password" name="password" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Confirm Password</label>
                        <input type="password" class="form-control" id="repeat_password" placeholder="Password" name="repeat_password" required>
                    </div>

                    <hr>
                    <div class="form-group text-center">
                        {{--<button type="submit" class="btn btn-success">Create Account</button> <br>--}}
                        <small id="fileHelp" class="form-text text-muted">By continuing to the next screen I confirm that I am at least 18 years of age and I agree to the terms in the <a href="javascript:void(0);">End User License Agreement.</a></small>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times-circle" aria-hidden="true"></i> Close</button>
                        <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Create Account</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
