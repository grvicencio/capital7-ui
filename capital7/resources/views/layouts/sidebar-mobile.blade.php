<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ url('/images/default_avatar.png') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>
                <a href="#"><i class="fa fa-user text-success" aria-hidden="true"></i></i> Profile Info</a>
            </div>
        </div>
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu tree" data-widget="tree">
            <!-- Optionally, you can add icons to the links -->
            <li class="">
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <i class="fa fa-bars" aria-hidden="true"></i>  <span>Menu</span>
                </a>
            </li>
            <li class="treeview active"><a href="/account/profile"> <i class="fa fa-home" aria-hidden="true"></i> <span> DASHBOARD</span> </a></li>
            <li class="treeview"><a href="/account/profile"><i class="fa fa-exchange" aria-hidden="true"></i> <span>TRANSFER </span></a></li>
            <li class="treeview"><a href="/account/profile"><i class="fa fa-money" aria-hidden="true"></i> <span>ADD FUNDS </span></a></li>
            <li class="treeview"><a href="/account/profile"><i class="fa fa-info-circle" aria-hidden="true"></i> <span>BILLING INFORMATION </span></a></li>
            {{--@isset($profile_menu) active @endisset--}}
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>