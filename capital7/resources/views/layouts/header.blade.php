<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

<title>{{ config('app.name') }}</title>
<link rel="icon" href="{{asset("images/WEB/CAPITAL 7  LOGO 1.png")}}">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- BOOTSTRAP v3.3.6 -->
<link rel="stylesheet" href="{{ asset("vendor/WEB/vendor/adminlte/bootstrap/css/bootstrap.min.css") }}">

<!-- FONTAWESOME -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- IONICONS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset("CRM/AdminLTE-2.4.2/bower_components/select2/dist/css/select2.min.css") }}">
<!-- THEME STYLE -->
<link rel="stylesheet" href="{{ asset('vendor/WEB/vendor//adminlte/dist/css/AdminLTE.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/WEB/vendor//adminlte/dist/css/skins/skin-blue.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/WEB/imagehover.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/WEB/nav_header.css') }}">
<link rel="stylesheet" href="{{ asset('css/WEB/custom.css') }}">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<link rel="icon" type="text/css" href="{{ asset('favicon.ico') }}">

<script src="https://www.google.com/recaptcha/api.js" async defer></script>