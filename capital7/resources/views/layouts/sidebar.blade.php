<nav id="sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar Menu -->
    <ul  class="sidebar-menu tree" data-widget="tree">
      <!-- Optionally, you can add icons to the links -->
      <li class=" @isset($profile_menu) active @endisset "><a href="/account/profile"><img class="sidebar-icon" src="{{asset("images/WEB/PROFILE.png")}}"> <span>Profile</span></a></li>
      <li class=" @isset($accdet_menu) active @endisset "><a href="/account/details"><img class="sidebar-icon" src="{{asset("images/WEB/ACCOUNT DETAILS.png")}} "><span>Account Details</span></a></li>
      <li class=" @isset($wallet_menu) active @endisset "><a href="/account/wallet"><img class="sidebar-icon" src="{{asset("images/WEB/WALLET2.png") }}">Wallet</a></li>
      <li class=" @isset($redeem_menu) active @endisset "><a href="/transaction/redeemcodes"><img class="sidebar-icon" src="{{ asset("images/WEB/REEDEM CODES.png")}} ">Reedem Codes</a></li>
      <li class=" @isset($history_menu) active @endisset "><a href="/transaction/history"><img class="sidebar-icon" src="{{ asset("images/WEB/TRANSACTIONS.png") }}">Transactions History</a></li>
      <li class=" @isset($security_menu) active @endisset "><a href="/account/security"><img class="sidebar-icon" src="{{ asset("images/WEB/SECURITY.png") }} ">Security</a></li>
      <li class=" @isset($notification_menu) active @endisset "><a href="/account/notification"><img class="sidebar-icon" src="{{asset("images/WEB/NOTIFICATION.png") }} "><span>Notification Preferences</span></a></li>
    </ul><!-- /.sidebar-menu -->
  </section>
  <!-- /.sidebar -->
</nav>