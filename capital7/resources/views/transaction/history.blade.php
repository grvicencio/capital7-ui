@extends('layouts.default')
@section('header_styles')
    <style>
        .flat-button {
            border-radius: 0px;
            height: 35px;
        }

        .img-thumbnail {
            width: 150px;
            height: 150px;
        }

        html, body {
            overflow-y: auto !important;
            overflow-x: hidden !important;
        }

        div#app {
            height: -webkit-fill-available;
        }

        input[type="checkbox"], input[type="radio"] {
            position: absolute;
            right: 9000px;
        }

        /*Check box*/
        input[type="checkbox"] + .label-text:before {
            content: "\f096";
            font-family: "FontAwesome";
            speak: none;
            font-style: normal;
            font-weight: normal;
            font-variant: normal;
            text-transform: none;
            line-height: 1;
            -webkit-font-smoothing: antialiased;
            width: 1em;
            display: inline-block;
            margin-right: 5px;
        }

        input[type="checkbox"]:checked + .label-text:before {
            content: "\f14a";
            color: #2980b9;
            animation: effect 250ms ease-in;
        }

        input[type="checkbox"]:disabled + .label-text {
            color: #aaa;
        }

        input[type="checkbox"]:disabled + .label-text:before {
            content: "\f0c8";
            color: #ccc;
        }

        /*Radio box*/

        input[type="radio"] + .label-text:before {
            content: "\f10c";
            font-family: "FontAwesome";
            speak: none;
            font-style: normal;
            font-weight: normal;
            font-variant: normal;
            text-transform: none;
            line-height: 1;
            -webkit-font-smoothing: antialiased;
            width: 1em;
            display: inline-block;
            margin-right: 5px;
        }

        input[type="radio"]:checked + .label-text:before {
            content: "\f192";
            color: #fb6b04;
            animation: effect 250ms ease-in;
        }

        input[type="radio"]:disabled + .label-text {
            color: #aaa;
        }

        input[type="radio"]:disabled + .label-text:before {
            content: "\f111";
            color: #ccc;
        }

        /*Radio Toggle*/

        .toggle input[type="radio"] + .label-text:before {
            content: "\f204";
            font-family: "FontAwesome";
            speak: none;
            font-style: normal;
            font-weight: normal;
            font-variant: normal;
            text-transform: none;
            line-height: 1;
            -webkit-font-smoothing: antialiased;
            width: 1em;
            display: inline-block;
            margin-right: 10px;
        }

        .toggle input[type="radio"]:checked + .label-text:before {
            content: "\f205";
            color: #16a085;
            animation: effect 250ms ease-in;
        }

        .toggle input[type="radio"]:disabled + .label-text {
            color: #aaa;
        }

        .toggle input[type="radio"]:disabled + .label-text:before {
            content: "\f204";
            color: #ccc;
        }

        @keyframes effect {
            0% {
                transform: scale(0);
            }
            25% {
                transform: scale(1.3);
            }
            75% {
                transform: scale(1.4);
            }
            100% {
                transform: scale(1);
            }
        }

        #btn_submit {
            background: #04142a;
            color: white;
            font-size: 12px;
            margin-right: 15px;
            width: 200px;
        }

        span.pull-right {
            margin-top: -7px;
        }

        .col-xs-12.col-sm-4.col-md-4.col-lg-2.sidebar-holder {
            top: 75px;
        }

        .sidebar-menu, .main-sidebar .user-panel, .sidebar-menu>li.header {
            white-space: nowrap;
            margin-left: -15px;
            overflow: hidden;
            margin-right: -15px;
            font-family: "gotham-book";
            font-size: 18px;
        }

        .skin-blue .sidebar-menu>li:hover>a, .skin-blue .sidebar-menu>li.active>a {
            color: #fff;
            background: #0e3670;
            border: none;
        }
        .panel{
            border-radius: 0px;
        }
        .box-body{
            font-family: gotham-book;
        }

    </style>
@endsection
@section('content')
    <div class='row'>
        <div id="div_message">
        </div>
        <div id="app" class="panel">
            <!-- /.box-header -->
            <!-- form start -->
            <div class="box-body">
                <div class="col-sm-12">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                        <img src="/images/{{ Session::get('path') }}">
                    @endif
                </div>
                {{--<form id="form_profile" class="form-horizontal" enctype="multipart/form-data" method="post"--}}
                      {{--action="/account/updateprofile">--}}
                    {{--{{ csrf_field() }}--}}
                    {{--<div class="col-sm-8">--}}
                        {{--<div class="row">--}}
                            {{--<div class="text-center pull-right">--}}
                                {{--<img id="avatar" name="avatar" src="/images/WEB/4B.png" class="img-thumbnail">--}}
                                {{--<br>--}}
                                {{--<a id="btn-change-avatar" class="label label-success"><i class="fa fa-upload"--}}
                                                                                         {{--aria-hidden="true"></i> Change--}}
                                    {{--Avatar</a>--}}
                                {{--<input type="file" id="open-file-image" class="form-control" name="image"--}}
                                       {{--style="display: none;" onchange="changeImage(this)" accept="image/*">--}}

                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="row">--}}
                            {{--<div class="form-group">--}}
                                {{--<label for="inputEmail3" class="col-sm-12 col-md-4 col-lg-3 control-label">Online--}}
                                    {{--ID</label>--}}
                                {{--<div class="col-sm-12 col-md-8 col-lg-9">--}}
                                    {{--<p class="lead">{{ $profile['id'] }}</p>--}}
                                    {{--<p class="lead">111</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                                {{--<label for="inputEmail3" class="col-sm-12 col-md-4 col-lg-3 control-label">First--}}
                                    {{--Name</label>--}}
                                {{--<div class="col-sm-12 col-md-8 col-lg-9">--}}
                                    {{--<input type="text" id="first_name" name="first_name" class="form-control"  value="{{ $profile['first_name'] }}" placeholder="" required>--}}
                                    {{--<input type="text" id="first_name" name="first_name" class="form-control"--}}
                                           {{--value="Apollon" placeholder="" required>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                                {{--<label for="inputEmail3" class="col-sm-12 col-md-4 col-lg-3 control-label">Last--}}
                                    {{--Name</label>--}}
                                {{--<div class="col-sm-12 col-md-8 col-lg-9">--}}
                                    {{--                                    <input type="text" id="last_name" name="last_name" class="form-control"  value="{{ $profile['last_name'] }}" placeholder="" required>--}}
                                    {{--<input type="text" id="last_name" name="last_name" class="form-control"--}}
                                           {{--value="Vladimir" placeholder="" required>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="form-group form-inline">--}}
                                {{--<label for="inputEmail3"--}}
                                       {{--class="col-sm-12 col-md-4 col-lg-3 control-label">Gender</label>--}}
                                {{--<div class="col-sm-12 col-md-8 col-lg-9 radio-inline">--}}

                                    {{--<label>--}}
                                        {{--<input type="radio" name="optgender" checked> <span--}}
                                                {{--class="label-text">Male</span>--}}
                                    {{--</label>--}}
                                    {{--<label>--}}
                                        {{--<input type="radio" name="optgender" checked> <span--}}
                                                {{--class="label-text">Female</span>--}}
                                    {{--</label>--}}

                                    {{--<span class="pull-right">--}}
                                        {{--<label for="inputEmail3" class="control-label" style="padding-bottom: 10px;">Date of Birth</label>--}}
                                        {{--                                        <input type="date" class="form-control" id="birth_date" name="birth_date" value="{{$profile['birth_date']}}">--}}
                                        {{--<input type="date" class="form-control" id="birth_date" name="birth_date"--}}
                                               {{--value="01/25/1990">--}}
                                    {{--</span>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                                {{--<label for="inputEmail3"--}}
                                       {{--class="col-sm-12 col-md-4 col-lg-3 control-label">Country</label>--}}
                                {{--<div class="col-sm-12 col-md-8 col-lg-9">--}}
                                    {{--<select id="country" name="country" class="form-control" required>--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group pull-right">--}}
                                {{--<button id="btn_submit" type="submit" class="btn btn-info flat-button"--}}
                                        {{--data-loading-text="Please wait...">Update Changes--}}
                                {{--</button>--}}
                            {{--</div>--}}
                            {{--<div class="form-group pull-right">--}}
                               {{--<p> Your Name, Sign-In ID, E-mail Address, Gender, Password, Security Question/Answer and Location--}}
                                   {{--information will be shared among the Pines group services that you have linked.--}}

                               {{--</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<!-- /.box-body -->--}}


                    {{--</div>--}}
                {{--</form>--}}
                <h3 class="title-header">Account Details</h3>
                <br>
                <br>
                <form class="form-inline">
                    <div class="form-group">
                        <label for="exampleInputName2">Online ID&nbsp;&nbsp;&nbsp;</label>
                        <input type="text" class="form-control form-input" id="exampleInputName2" placeholder="Jane Doe">
                    </div>
                    <br>
                    <br>
                    <div class="form-group">
                        <label for="exampleInputName2">First Name</label>
                        <input type="text" class="form-control form-input" id="exampleInputName2" placeholder="Jane Doe">
                    </div>
                    <br>
                    <br>
                    <div class="form-group">
                        <label for="exampleInputEmail2">Last Name </label>
                        <input type="email" class="form-control form-input" id="exampleInputEmail2" placeholder="jane.doe@example.com">
                    </div>
                    <br>
                    <br>
                    <div class="form-group">
                        <label for="exampleInputEmail2">Gender&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;</label>
                        <select class="form-control form-input" style="width:200px !important; padding-right: 10px;">
                            <option></option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail2" style="margin-left: 10px; !important;">Birthdate</label>
                        <input type="date" class="form-control form-input" id="birth_date" name="birth_date" value="{{$profile['birth_date']}}"  style="width:190px !important;">
                    </div>
                    <br>
                    <br>
                    <div class="form-group">
                        <label for="exampleInputEmail2">Country&nbsp;&nbsp;&nbsp; &nbsp;</label>
                        <select class="form-control form-input" style="width:200px !important; padding-right: 10px;">
                            <option></option>
                        </select>
                        <br>
                        <br>

                    </div>
                    <br>
                    <br>
                    <div class="form-group  form-input" style="width:590px !important;">
                        <button id="btn_submit" type="submit" class="btn btn-info flat-button pull-right"
                                data-loading-text="Please wait...">Update Setting
                        </button>
                    </div>
                    <br>
                    <br>
                    <div class="form-group ">
                    <p style="font-family: gotham-book;"> Your Name, Sign-In ID, E-mail Address, Gender, Password, Security Question/Answer and <br>Location
                    information will be shared among the Pines group services that you have linked.

                    </p>
                    </div>
                </form>
                <!-- /.box-body -->

                <!-- /.box-footer -->

            </div>

            <!-- /.box -->

        @endsection

        @section('footer_scripts')

            <!-- ClubNine -->
                {{--    <script src="{{ config('crm.url') . '/ClubNine-1.0.0/js/countries.js' }}"></script>--}}

                <script type="text/javascript">
                    $(document).ready(function () {
                        $('#btn-change-avatar').click(function () {
                            $('#open-file-image').click();
                        });

                        $('#btn-save').click(function () {
                            var profile_data = new FormData($('form#form-update-avatar')[0]);



                            {{--$.ajax({--}}
                            {{--type: 'POST',--}}
                            {{--url:" /account/profile/update/{{$profile['id']}}",--}}
                            {{--data:avatar_data,--}}
                            {{--dataType: 'json',--}}
                            {{--success:function(data) {--}}
                            {{--alert("Profile Successfully Updated!");--}}
                            {{--},--}}
                            {{--});--}}


                        });
                    });


                    function changeImage(input) {
                        var url = input.value;
                        var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();

                        if (input.files && input.files[0] && (ext == "png" || ext == "jpeg" || ext == "jpg")) {
                            if ((input.files[0].size / 1024) <= 2000) {

                                // console.log(input.files[0]);

                                var reader = new FileReader();
                                reader.onload = function (e) {
                                    $('#avatar').attr('src', e.target.result);
                                };
                                reader.readAsDataURL(input.files[0]);
                                var avatar_data = new FormData($('form#update-avatar')[0]);
                                swal(
                                    'Success!',
                                    'Please click save changes to save your avatar.',
                                    'success'
                                )
                            } else {
                                swal(
                                    'Error!',
                                    'Maximum image size is 2MB.',
                                    'error'
                                )

                            }

                        } else {
                            swal(
                                'Error!',
                                'Invalid image format.',
                                'error'
                            )
                        }

                    }

                    $('img').error(function () {

                        {{--@if(isset($profile_image_file))--}}
                        {{--$(".profile-image").attr("src", "{{ $profile_image_file }}");--}}
                        {{--@else--}}
                        {{--$(this).attr('src', "{{ asset('images/default_avatar.png') }}");--}}
                        {{--@endif--}}


                    });
                    @if(isset($profile_image_file))

                    $(".profile-image").attr("src", "{{$profile_image_file}}");
                    @else
                    $(this).attr('src', "{{ asset('images/default_avatar.png') }}");
                    @endif



                    $('#birth_date').attr('min', '1900-01-01');


                </script>


                <script>

                    // var select = document.getElementById("country");
                    // // Optional: Clear all existing options first:
                    // select.innerHTML = "";
                    // // Populate list with options:
                    // for (var i = 0; i < countries.length; i++) {
                    //     var opt = countries[i];
                    //     select.innerHTML += "<option value=\"" + opt.id + "\">" + opt.text + "</option>";
                    // }

                            {{--        var selectedcountry = "{{$profile['country']}}";--}}
                    var selectedcountry = "Philippines";
                    $('#country').val(selectedcountry.trim());

                    @isset($profile['gender'])
                    $('#gender').val("{{$profile['gender']}}");
                            @endisset


                    var dtToday = new Date();
                    var month = dtToday.getMonth() + 1;// jan=0; feb=1 .......
                    var day = dtToday.getDate();
                    var year = dtToday.getFullYear() - 18;
                    if (month < 10)
                        month = '0' + month.toString();
                    if (day < 10)
                        day = '0' + day.toString();
                    var maxDate = year + '-' + month + '-' + day;
                    var dateToday = (year + 18) + '-' + month + '-' + day;
                    $('#birth_date').attr('max', maxDate);

                    function validate_birthdate(e, dateToday) {
                        e.target.setCustomValidity('');
                        if ($('#birth_date').val() > dateToday || ($('#birth_date').val() <= $('#birth_date').attr('min') && $('#birth_date').val() != "")) {
                            e.target.setCustomValidity("Invalid date!");
                        }
                        else if ($('#birth_date').val() > maxDate && $('#birth_date').val() != "") {
                            e.target.setCustomValidity("You must be 18 years old and above!");
                            return;
                        }
                    }

                    $('#birth_date').on('focusout', function (e) {
                        validate_birthdate(e, dateToday);
                    });
                    $('#birth_date').on('change', function (e) {
                        validate_birthdate(e, dateToday);
                    });


                    $('#form_profile').on('submit', function (e) {
                        e.preventDefault();
                        var profile_data = new FormData(this);

                        {{--$.ajax({--}}
                        {{--type: 'POST',--}}
                        {{--url:" /account/profile/update/{{$profile['id']}}",--}}
                        {{--data:avatar_data,--}}
                        {{--dataType: 'json',--}}
                        {{--success:function(data) {--}}
                        {{--alert("Profile Successfully Updated!");--}}
                        {{--},--}}
                        {{--});--}}

                        // var formData = $('#form_profile');
                        var btn = $(this).find(':submit').button('loading');
                        $.ajax({
                            url: "/account/updateprofile",
                            type: "post",
                            // dataType: 'application/json',
                            data: profile_data,
                            // cache: false,
                            contentType: false,
                            processData: false,
                            success: function (data) {

                                $('#div_message').html('<div class="alert alert-warning alert-dismissible">\n' +
                                    '                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>\n' +
                                    data +
                                    '            </div>');
                                btn.button('reset');
                                console.log(data);

                            },
                            error: function (error) {
                                console.log(error);
                                btn.button('reset');

                            }

                        });
                    });
                </script>
@endsection