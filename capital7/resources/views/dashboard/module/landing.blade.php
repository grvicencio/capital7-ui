<style>
    .wrapper{
        height: auto;
        min-height: -webkit-fill-available !important;
    }

    .div-content {
        height: 100%;
        background: #091629 !important;
    }
    .skin-blue .wrapper, .skin-blue .main-sidebar, .skin-blue .left-side, html {
        background-color: #EEEEEE;
        overflow: hidden !important;
    }

    .div-content {
        height: 100%;
        background: #eeeeee !important;
    }
    .main-content-holder {
        height: 100vh;

        background-color: transparent;
        background: url('{{ asset('images/WEB/background.png') }}') no-repeat left -48px;
        /* background-color: #cccccc; */
        background-repeat: no-repeat;
        background-size: cover;
        background-size: 100% 100%;
        margin-left: 0px !important;
        margin-top: 0px !important;;
    }


    div#main_content {
        padding-top: 12%;
    }
</style>

<div id="main_content" class="row text-center">
    <span style="font-family: 'gotham-bold'; color:#baac4a; font-size: 50px">BUY AND SALE <br>
CRYPTO CURRENCY ASSETS!</span><br>
    <span style="font-family: 'gotham-book'; color:#f3f3f3; font-size: 20px"> Real time trading. Trade Currencies faster than ever, low fees. </span>

</div>

