<style>
    .wrapper{
        height: auto;
        min-height: -webkit-fill-available !important;
    }

    .div-content {
        height: 100%;
        background: #091629 !important;
    }
    .skin-blue .wrapper, .skin-blue .main-sidebar, .skin-blue .left-side, html {
        background-color: #EEEEEE;
        overflow: hidden !important;
    }

    .div-content {
        height: 100%;
        background: #eeeeee !important;
    }
</style>

<div class="panel cards col-xs-12 col-md-2 ">
    <a href="{{ route('open.application','Baccarat')}}" style="color: black;" target="_blank">
        <div class="panel-body">
            <img class="app-img" src="{{ asset("images/WEB/CLUB9 LOGO.png") }}">
            <br>
            <b class="app-title"> Clubnine </b><br>
            <p class="app-developer"> Ninepine Tech </p>
            <p class="app-description ">Chips are our in-game currency which you can use to play any of the casino games on the gaming floor. </p>
        </div>
    </a>
</div>


<div class="col-xs-12 col-md-2 panel cards">
    <div class="panel-body">
        <a href="{{ route('open.application','RakeBackAsia')}}" style="color: black;" target="_blank">
 
            <img class="app-img" src="{{ asset("images/WEB/RAKEBACK LOGO.png") }}">
        <br>
        <b class="app-title"> Rakeback </b><br>
        <p class="app-developer"> Ninepine Tech </p>
        <p class="app-description ">Best rakeback deals, safe access to all poker networks and comprehensive solutions for players. </p>

        </a>
    </div>
</div>

<div class="panel cards col-xs-12 col-md-2 ">
   <a href="{{ route('open.application','Capital7')}}" style="color: black;" target="_blank">
    <div class="panel-body">
      
         <img class="app-img" src="{{ asset("images/WEB/CAPITAL 7  LOGO 1.png") }}">
       
        <br>
        <b class="app-title"> Capital 7 </b><br>
        <p class="app-developer"> Ninepine Tech </p>
        <p class="app-description "> Real time trading. Trade Currencies faster than ever, low fees. </p>
    </div>
    </a>
</div>


<div class="panel cards col-xs-12 col-md-2 panel-add">
    <a href="#" data-toggle="modal" data-target="#modal_add_application" data-backdrop="static" data-keyboard="false" style="color: black;">
    <div class="panel-body" style="width: 100%; height: 100%;">
    </div>
    </a>
</div>
