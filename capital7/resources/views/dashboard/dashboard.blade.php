@extends('layouts.default')

@section('header_styles')
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css"
          rel="stylesheet"/>

    <style>
        .abbr {
            border-bottom: 3px dotted #DDBF20 !important;
            font-size: medium;
        }

        .abbr span.abbr-decimal {
            font-size: small;
        }

        .btn-container {
            display: block;
            margin-top: -35px;
            position: absolute;
            right: 15px;
            visibility: hidden;
            width: 0px;

            background: #F5F5F5;
        }

        .btn-container .btn {
            padding: 10px 14px !important;
        }

        pre {
            margin: 0px !important;
        }

        .text-default {
            color: #BBB;
        }

        .actionticket .table td:first-child {
            width: 20% !important;
            color: #444 !important;
            font-weight: bold;
        }

        .actionticket .table td:nth-child(1) {
            width: 12% !important;
            text-align: center !important;
        }

        .actionticket .box-body {
            min-height: 370px;
        }

        .actionticket .table td:nth-child(2) {
            width: 15% !important;
            text-align: center !important;
        }

        .actionticket .table td:nth-child(3) {
            width: 40% !important;
            text-align: left !important;
        }

        .actionticket .table td:nth-child(4) {
            width: 5% !important;
            text-align: center !important;
        }

        .actionticket .table td:last-child {
            width: 5% !important;
            text-align: center !important;
        }

        .actionticket .table td:last-child a {
            font-weight: normal !important;
        }

        .actionticket .table td {
            padding: 5px 0px !important
        }

        .actionticket .table th, .actionticket .table td {
            text-align: center !important;
        }

        .btn-xs {
            padding: 1px 5px !important;
        }

        .actionticket .table td, .text-default {
            color: #0a0a0a !important;
        }

        code {
            background-color: #ffffff;
            color: #000000;
        }

        .panel-body p {
            font-size: 16px;
        }

        .dotted-line {
            border: 1px dotted #ff0000;
            border-style: none none dotted;
            color: #fff;
            background-color: #fff;
        }

        #add-currency {
            width: 15em;
        }

        #image-popup-modal {
            z-index: 999999999;
        }

        #img-popup {
            max-width: 100%;
            max-height: 100%;
            height: auto;
            width: auto;
        }

        .show-more-tickets {
            width: 150px;
        }

        p.text-muted.text-center.no-active-offers {
            margin: 20px !important;
        }



        body {
            height: 100%;
        }

        .carousel,
        .item,
        .active {
            height: 100%;
        }


        .carousel-inner {
            height: 100%;
        }


        .carousel,
        .item,
        .active {
            height: 100%;
        }


        .carousel-inner {
            height: 100%;
        }

        /* Background images are set within the HTML using inline CSS, not here */

        .fill {
            width: 100%;
            height: 80vh;
            background-position: center;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            background-size: cover;
            -o-background-size: cover;
        }

    </style>

@endsection

@section('content')
    <div class="row">
        @if(Session::has('message'))
            <div class="row">
                <div class="box box-primary box-warning">
                    <div class="box-body text-warning">
                        {{ Session::get('message') }}
                    </div>
                </div>
            </div>

        @endif


    </div>
    <div class="main-content-holder">
        @if($islogin==false)
            @include('dashboard.module.mainContent')
        @else
            @include('dashboard.module.landing')
        @endif


    </div>

@endsection

@section('footer_scripts')
    <script type="text/javascript"
            src="{{ asset('bower_components/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    @isset($messageAlertResult)
            <script>
                swal({
                    title: "{{$messageAlertResult}}".toUpperCase(),
                    text: "{{$messageAlertMessage}}",
                    type: "{{$messageAlertResult == "success"  ? "success" : "error" }}" ,
                    confirmButtonText: 'OK'
                });
            </script>
    @endisset
@endsection

