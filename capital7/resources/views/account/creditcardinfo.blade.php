@extends('layouts.default')
@section('header_styles')
<style>

.devices-container {
  padding-right: 0;
  padding-left: 0;
}
.device-container {
  padding-right: 7px;
  padding-left: 7px;
}

.device-header {
  font-weight: bold;
  font-size: 110%;
}
.phone-img-holder{
  background-color: white;
}
.info-box-icon {
  background: white;
}
.box.box-solid>.box-header {
  color: #fff;
  background: #14805e;
  background-color: #14805e;
  border-color: #14805e;

}

.content-header>h1 {

  color: white;
}
.box.box-solid.box-primary {
  border: 1px solid #14805e;
}
.skin-blue .main-header .navbar .nav>li>a:hover, .skin-blue .main-header .navbar .nav>li>a:active, .skin-blue .main-header .navbar .nav>li>a:focus, .skin-blue .main-header .navbar .nav .open>a, .skin-blue .main-header .navbar .nav .open>a:hover, .skin-blue .main-header .navbar .nav .open>a:focus, .skin-blue .main-header .navbar .nav>.active>a
{
  color: white;
  background: rgba(4, 58, 36, 1);
  border-radius: 10px;
}
.required{
  color: red;
  font-weight: bold;
}
input{

      text-align: center;
}
.btn-success {
    background-color: #14805e;
    border-color: #14805e;
}

.col-sm-12.input-group.text-center {
  display: flex;
  align-items: center;
  justify-content: center;
}
label {
  display: inline-block;
  max-width: 100%;
  margin-bottom: 5px;
  font-weight: 700;
  font-size: 11px;
}

</style>
@endsection
@section('content')
  <div class="row">
    <div class="col-md-8">
      <div class="box box-solid">
        <div class="box-header with-border">
          <!-- <h3 class="box-title">Credit Card Information</h3> -->
        </div>
        <div class="box-body" id="device_body">
          <div class="form-group text-center">
            <div class="text-center">
              <img src="{{ url('/images/card-logos.jpg') }}" style="width:400px; height:100px;">
            </div>

          </div>

          <div class="form-group">
            <div class="col-sm-6">
              <label for="lgFormGroupInput">Credit or debit card number: </label>
              <input type="text" class="form-control form-control-sm" id="card_number" placeholder="xxxxxxxxxxxxxx">
            </div>
            <div class="col-sm-4">

              <div class="col-sm-12" style="padding-left: 0px;">
                <label for="lgFormGroupInput"><span class="required">* </span> Expiration date:  </label>

              </div>

              <div class="col-sm-12 input-group text-center">
                <select style="width:60%;" class="form-control" name="expiry_month" id="expiry_month">
                  <option></option>
                  <option value="01" selected="">Jan (01)</option>
                  <option value="02">Feb (02)</option>
                  <option value="03">Mar (03)</option>
                  <option value="04">Apr (04)</option>
                  <option value="05">May (05)</option>
                  <option value="05">May (05)</option>
                  <option value="06">June (06)</option>
                  <option value="07">July (07)</option>
                  <option value="08">Aug (08)</option>
                  <option value="09">Sep (09)</option>
                  <option value="10">Oct (10)</option>
                  <option value="11">Nov (11)</option>
                  <option value="12">Dec (12)</option>
                </select>
                <select style="width:40%;" class="form-control" name="expiry_year">
                  <option value="13">2013</option>
                  <option value="14">2014</option>
                  <option value="15">2015</option>
                  <option value="16">2016</option>
                  <option value="17">2017</option>
                  <option value="18">2018</option>
                  <option value="19">2019</option>
                  <option value="20">2020</option>
                  <option value="21">2021</option>
                  <option value="22">2022</option>
                  <option value="23">2023</option>
                </select>
              </div>


            </div>
            <div class="col-sm-2">
              <label for="lgFormGroupInput"> <span class="required">* </span> Security Code: </label>
              <input type="text" class="form-control" text-align="center" id="security_code" placeholder="XXX">


            </div>



          </div>
          <br><br><br>
          <hr>
          <div class="pull-right">
            <button id="btn_save" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
          </div>





          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.box -->

    </div>



  </div>
@endsection
