<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>NinePine Platform</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <style type="text/css">

        div#forgot-password h1 a {
            box-shadow: none!important;
            height: 170px;
            width: 100% !important;
            background-image: url({{asset("images/NINEPINE-LOGO.png")}}) !important;
            background-repeat: no-repeat;
            -webkit-background-size: 200px 170px;
            background-size: 200px 170px;
            background-position: center top;
            background-repeat: no-repeat;
            color: #999;
            /* height: 150px; */
            font-size: 20px;
            font-weight: 400;
            line-height: 1.3em;
            margin: 0 auto 25px;
            padding: 0;
            text-decoration: none;
            width: 80px;
            text-indent: -9999px;
            outline: 0;
            overflow: hidden;
            display: block;


        }
        #forgot-password {

            padding: 0 0 0;
            margin: auto;
            float: none;

        }
        .message{
            background-color: #fd875c;
            border-color: #fd875c;
            font-weight: 500;
            color: #FFF;
            box-shadow: none;
            padding: 10px;
        }
        .btn{
            border-radius: 0px;
            width: 80%;
        }

        form {
            margin-top: 20px;
            margin-left: 0;
            padding: 26px 26px 2px;
            font-weight: 400;
            overflow: hidden;
            background: #fff;
            -webkit-box-shadow: 0 0 10px rgba(0,0,0,.13);
            box-shadow: 0 0 10px rgba(0,0,0,.13);
        }

        hr{
            border-top: 1px dashed #8c8b8b;
            border-bottom: 1px dashed #fff;
        }
        input.btn.btn-primary {
            border-radius: 0px;
        }
        .form-control{
            border-radius: 0px;
        }
        .strike {
            display: block;
            text-align: center;
            overflow: hidden;
            white-space: nowrap;

            font-size: 12px;
        }

        .strike > span {
            position: relative;
            display: inline-block;
        }

        .strike > span:before,
        .strike > span:after {
            content: "";
            position: absolute;
            top: 50%;
            width: 9999px;
            /* Here is the modification */
            border-top: 1px dashed #8c8b8b;
            border-bottom: 1px dashed #fff;
        }

        .strike > span:before {
            right: 100%;

        }

        .strike > span:after {
            left: 100%;
            margin-left: 5px;
            margin-bottom: 5px;
        }
        .strike{
            margin-right: 15px;
        }
        div.alert.alert-danger {
            text-overflow: ellipsis;
            word-wrap: break-word;
        }
    </style>
</head>
<body>
<div class="container-fluid">




    <div id="forgot-password" class="col-xs-12 col-sm-2">
        <h1>
            <a href="#" title="Ninepine Tech">Ninepine Tech</a>
        </h1>
        <hr>
        @if(isset($result))
             <p class="message">{{$message}}</p>
        @else
        <p class="message">Please enter your new password.</p>
            <form name="password_reset" id="password_reset" action="" method="post">
                <div id="message" class="alert alert-warning" hidden> </div>
                {{ csrf_field() }}
                <input type="hidden" id="token" name="token" value="{{$token}}">
                <input type="hidden" id="email" name="email" value="{{base64_decode($email)}}">
                <div class="form-group">
                    <label for="secret-question-answer">New Password: </label>
                    <input type="password" name="password" id="password" class="form-control" placeholder="New password" required>
                </div>
                <div class="form-group">
                    <label for="secret-question-answer">Confirm Password: </label>
                    <input type="password" name="repeat_password" id="repeat_password" required class="form-control" placeholder="Confirm password">
                </div>
                <p class="text-center">
                    <button type="submit" id="btn_submit" class="btn btn-success" > <i class="fa fa-paper-plane-o" aria-hidden="true"></i> Submit</button>
                </p>
            </form>

        @endif


        <hr>

    </div>

</div>
<script src="https://code.jquery.com/jquery-2.x-git.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script>
    $('#password_reset').submit( function(e) {
        e.preventDefault();
        var formData = $('#password_reset').serialize();
        $.ajax({
            url: "/reset_password_email",
            type: "post",
            data: formData,
            success: function (data) {
                data = (jQuery.parseJSON(data));
                if(data.result == 'failed'){
                    $('#message').html(data.message);
                    $('#message').show();
                }else
                $('#password_reset').html( '<div id="message" class="alert alert-warning">' + data.message + ' click <a href="{{url('/')}}">here</a> to login  ' + '</div>');
            },
            error: function (error) {
                $('#message').html("Invalid email/username.");
                $('#message').show();
            }
        });

    });

</script>

</body>


</html>