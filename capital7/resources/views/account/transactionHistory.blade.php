@extends('layouts.default')
@section('header_styles')
    <style>
        .flat-button {
            border-radius: 0px;
            height: 35px;
        }

        .img-thumbnail {
            width: 150px;
            height: 150px;
        }

        html, body {
            overflow-y: auto !important;
            overflow-x: hidden !important;
        }

        div#app {
            height: -webkit-fill-available;
        }

        input[type="checkbox"], input[type="radio"] {
            position: absolute;
            right: 9000px;
        }

        /*Check box*/
        input[type="checkbox"] + .label-text:before {
            content: "\f096";
            font-family: "FontAwesome";
            speak: none;
            font-style: normal;
            font-weight: normal;
            font-variant: normal;
            text-transform: none;
            line-height: 1;
            -webkit-font-smoothing: antialiased;
            width: 1em;
            display: inline-block;
            margin-right: 5px;
        }

        input[type="checkbox"]:checked + .label-text:before {
            content: "\f14a";
            color: #2980b9;
            animation: effect 250ms ease-in;
        }

        input[type="checkbox"]:disabled + .label-text {
            color: #aaa;
        }

        input[type="checkbox"]:disabled + .label-text:before {
            content: "\f0c8";
            color: #ccc;
        }

        /*Radio box*/

        input[type="radio"] + .label-text:before {
            content: "\f10c";
            font-family: "FontAwesome";
            speak: none;
            font-style: normal;
            font-weight: normal;
            font-variant: normal;
            text-transform: none;
            line-height: 1;
            -webkit-font-smoothing: antialiased;
            width: 1em;
            display: inline-block;
            margin-right: 5px;
        }

        input[type="radio"]:checked + .label-text:before {
            content: "\f192";
            color: #fb6b04;
            animation: effect 250ms ease-in;
        }

        input[type="radio"]:disabled + .label-text {
            color: #aaa;
        }

        input[type="radio"]:disabled + .label-text:before {
            content: "\f111";
            color: #ccc;
        }

        /*Radio Toggle*/

        .toggle input[type="radio"] + .label-text:before {
            content: "\f204";
            font-family: "FontAwesome";
            speak: none;
            font-style: normal;
            font-weight: normal;
            font-variant: normal;
            text-transform: none;
            line-height: 1;
            -webkit-font-smoothing: antialiased;
            width: 1em;
            display: inline-block;
            margin-right: 10px;
        }

        .toggle input[type="radio"]:checked + .label-text:before {
            content: "\f205";
            color: #16a085;
            animation: effect 250ms ease-in;
        }

        .toggle input[type="radio"]:disabled + .label-text {
            color: #aaa;
        }

        .toggle input[type="radio"]:disabled + .label-text:before {
            content: "\f204";
            color: #ccc;
        }

        @keyframes effect {
            0% {
                transform: scale(0);
            }
            25% {
                transform: scale(1.3);
            }
            75% {
                transform: scale(1.4);
            }
            100% {
                transform: scale(1);
            }
        }

        #
        {
            background: #04142a
        ;
            color: white
        ;
            font-size: 12px
        ;
            margin-right: 15px
        ;
            width: 200px
        ;
        }

        span.pull-right {
            margin-top: -7px;
        }

        .col-xs-12.col-sm-4.col-md-4.col-lg-2.sidebar-holder {
            top: 75px;
        }

        .sidebar-menu, .main-sidebar .user-panel, .sidebar-menu > li.header {
            white-space: nowrap;
            margin-left: -15px;
            overflow: hidden;
            margin-right: -15px;
            font-family: "gotham-book";
            font-size: 18px;
        }

        .skin-blue .sidebar-menu > li:hover > a, .skin-blue .sidebar-menu > li.active > a {
            color: #fff;
            background: #0e3670;
            border: none;
        }

        .panel {
            border-radius: 0px;
        }

        .box-body {
            font-family: gotham-book;
        }

        .form-input {
            margin-left: 30px;
            width: 252px !important;
        }

        button.btn.btn-info.flat-button {
            background: #04142a;
            border-radius: 8px;
        }


    </style>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.standalone.css">
@endsection
@section('content')
    <div class='row'>
        <div id="div_message">
        </div>
        <div id="app" class="panel">
            <!-- /.box-header -->
            <!-- form start -->
            <div class="box-body">
                <div class="col-sm-12">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                        <img src="/images/{{ Session::get('path') }}">
                    @endif
                </div>

                <h3 class="title-header">Transaction History</h3>
                <br>
                <br>
                <form class="form-inline">
                    <div class="form-group">
                        <label for="">Account</label>
                        <input type="text" class="form-control form-input control-label" id="" placeholder=""
                               value="{{ $profile['email'] }}" style="margin-left:57px !important;" disabled>
                        <img src="{{asset("images/WEB/questionmark.png")}}" style="width:25px;">
                    </div>
                    <br>
                    <br>
                    <div class="form-group">

                        <label for="">Date Range:</label>
                        <div class="form-group">
                            <div class='input-group date' id='datetimepicker1'>
                                <input type='text' class="form-control form-input dtp control-label" id="valid_thru"
                                       name="valid_thru" style="margin-left:32px !important; width: 250px !important;"
                                       pattern="\d{1,2}/\d{4}" required
                                       value="@if( isset($cardinfo['data'][0]['valid_thru']))  {{ $cardinfo['data'][0]['valid_thru'] }} @else {{ old('valid_thru') }} @endif">
                                <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar open-datetimepicker"></span>
                    </span>
                            </div>
                        </div>
                        to
                        <div class='input-group date' id='datetimepicker1'>
                            <input type='text' class="form-control form-input dtp control-label" id=""
                                   name="" style="margin-left:32px !important; width: 250px !important;"
                                   pattern="\d{1,2}/\d{4}" required
                                   value="@if( isset($cardinfo['data'][0]['valid_thru']))  {{ $cardinfo['data'][0]['valid_thru'] }} @else {{ old('valid_thru') }} @endif">
                            <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar open-datetimepicker"></span>
                    </span>
                        </div>
                        <button id="" type="submit" class="btn btn-info flat-button"
                                data-loading-text="Please wait...">Update
                        </button>
                    </div>

                    <br>
                    <br>
                    <div class="form-group">
                        <table id="example" class="display" style="width:726px">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Description</th>
                                <th>Acount</th>
                                <th>Amount</th>
                            </tr>

                            </thead>

                            <tbody>
                            <tr>
                                <td>Sample</td>
                                <td>Sample</td>
                                <td>Sample</td>
                                <td>Sample</td>
                            </tr>

                            </tbody>


                        </table>
                    </div>

                    <br>
                    <br>

                </form>
                <!-- /.box-body -->

                <!-- /.box-footer -->

            </div>

            <!-- /.box -->

        @endsection

        @section('footer_scripts')

            <!-- ClubNine -->
                <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
                <script>
                    $(document).ready(function () {
                        $('#example').DataTable();
                        $('.dtp').datepicker({
                            pickTime: false,
                            format: 'mm/dd/yyyy'
                        });
                        $('#valid_thru').click(function (event) {
                            $('.open-datetimepicker').click();
                        });
                    });
                </script>
                <script type="text/javascript">
                    $(document).ready(function () {
                        $('#btn-change-avatar').click(function () {
                            $('#open-file-image').click();
                        });

                        $('#btn-save').click(function () {
                            var profile_data = new FormData($('form#form-update-avatar')[0]);



                            {{--$.ajax({--}}
                            {{--type: 'POST',--}}
                            {{--url:" /account/profile/update/{{$profile['id']}}",--}}
                            {{--data:avatar_data,--}}
                            {{--dataType: 'json',--}}
                            {{--success:function(data) {--}}
                            {{--alert("Profile Successfully Updated!");--}}
                            {{--},--}}
                            {{--});--}}


                        });
                    });


                    function changeImage(input) {
                        var url = input.value;
                        var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();

                        if (input.files && input.files[0] && (ext == "png" || ext == "jpeg" || ext == "jpg")) {
                            if ((input.files[0].size / 1024) <= 2000) {

                                // console.log(input.files[0]);

                                var reader = new FileReader();
                                reader.onload = function (e) {
                                    $('#avatar').attr('src', e.target.result);
                                };
                                reader.readAsDataURL(input.files[0]);
                                var avatar_data = new FormData($('form#update-avatar')[0]);
                                swal(
                                    'Success!',
                                    'Please click save changes to save your avatar.',
                                    'success'
                                )
                            } else {
                                swal(
                                    'Error!',
                                    'Maximum image size is 2MB.',
                                    'error'
                                )

                            }

                        } else {
                            swal(
                                'Error!',
                                'Invalid image format.',
                                'error'
                            )
                        }

                    }

                    $('img').error(function () {

                        {{--@if(isset($profile_image_file))--}}
                        {{--$(".profile-image").attr("src", "{{ $profile_image_file }}");--}}
                        {{--@else--}}
                        {{--$(this).attr('src', "{{ asset('images/default_avatar.png') }}");--}}
                        {{--@endif--}}


                    });
                    @if(isset($profile_image_file))

                    $(".profile-image").attr("src", "{{$profile_image_file}}");
                    @else
                    $(this).attr('src', "{{ asset('images/default_avatar.png') }}");
                    @endif



                    $('#birth_date').attr('min', '1900-01-01');


                </script>


                <script>

                    // var select = document.getElementById("country");
                    // // Optional: Clear all existing options first:
                    // select.innerHTML = "";
                    // // Populate list with options:
                    // for (var i = 0; i < countries.length; i++) {
                    //     var opt = countries[i];
                    //     select.innerHTML += "<option value=\"" + opt.id + "\">" + opt.text + "</option>";
                    // }

                            {{--        var selectedcountry = "{{$profile['country']}}";--}}
                    var selectedcountry = "Philippines";
                    $('#country').val(selectedcountry.trim());

                    @isset($profile['gender'])
                    $('#gender').val("{{$profile['gender']}}");
                            @endisset


                    var dtToday = new Date();
                    var month = dtToday.getMonth() + 1;// jan=0; feb=1 .......
                    var day = dtToday.getDate();
                    var year = dtToday.getFullYear() - 18;
                    if (month < 10)
                        month = '0' + month.toString();
                    if (day < 10)
                        day = '0' + day.toString();
                    var maxDate = year + '-' + month + '-' + day;
                    var dateToday = (year + 18) + '-' + month + '-' + day;
                    $('#birth_date').attr('max', maxDate);

                    function validate_birthdate(e, dateToday) {
                        e.target.setCustomValidity('');
                        if ($('#birth_date').val() > dateToday || ($('#birth_date').val() <= $('#birth_date').attr('min') && $('#birth_date').val() != "")) {
                            e.target.setCustomValidity("Invalid date!");
                        }
                        else if ($('#birth_date').val() > maxDate && $('#birth_date').val() != "") {
                            e.target.setCustomValidity("You must be 18 years old and above!");
                            return;
                        }
                    }

                    $('#birth_date').on('focusout', function (e) {
                        validate_birthdate(e, dateToday);
                    });
                    $('#birth_date').on('change', function (e) {
                        validate_birthdate(e, dateToday);
                    });


                    $('#form_profile').on('submit', function (e) {
                        e.preventDefault();
                        var profile_data = new FormData(this);

                        {{--$.ajax({--}}
                        {{--type: 'POST',--}}
                        {{--url:" /account/profile/update/{{$profile['id']}}",--}}
                        {{--data:avatar_data,--}}
                        {{--dataType: 'json',--}}
                        {{--success:function(data) {--}}
                        {{--alert("Profile Successfully Updated!");--}}
                        {{--},--}}
                        {{--});--}}

                        // var formData = $('#form_profile');
                        var btn = $(this).find(':submit').button('loading');
                        $.ajax({
                            url: "/account/updateprofile",
                            type: "post",
                            // dataType: 'application/json',
                            data: profile_data,
                            // cache: false,
                            contentType: false,
                            processData: false,
                            success: function (data) {

                                $('#div_message').html('<div class="alert alert-warning alert-dismissible">\n' +
                                    '                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>\n' +
                                    data +
                                    '            </div>');
                                btn.button('reset');
                                console.log(data);

                            },
                            error: function (error) {
                                console.log(error);
                                btn.button('reset');

                            }

                        });
                    });
                </script>
@endsection