@extends('layouts.default')
@section('header_styles')
    <style>
        .flat-button {
            border-radius: 0px;
            height: 35px;
        }

        .img-thumbnail {
            width: 150px;
            height: 150px;
        }

        html, body {
            overflow-y: auto !important;
            overflow-x: hidden !important;
        }

        div#app {
            height: -webkit-fill-available;
        }

        input[type="checkbox"], input[type="radio"] {
            position: absolute;
            right: 9000px;
        }

        /*Check box*/
        input[type="checkbox"] + .label-text:before {
            content: "\f096";
            font-family: "FontAwesome";
            speak: none;
            font-style: normal;
            font-weight: normal;
            font-variant: normal;
            text-transform: none;
            line-height: 1;
            -webkit-font-smoothing: antialiased;
            width: 1em;
            display: inline-block;
            margin-right: 5px;
        }

        input[type="checkbox"]:checked + .label-text:before {
            content: "\f14a";
            color: #2980b9;
            animation: effect 250ms ease-in;
        }

        input[type="checkbox"]:disabled + .label-text {
            color: #aaa;
        }

        input[type="checkbox"]:disabled + .label-text:before {
            content: "\f0c8";
            color: #ccc;
        }

        /*Radio box*/

        input[type="radio"] + .label-text:before {
            content: "\f10c";
            font-family: "FontAwesome";
            speak: none;
            font-style: normal;
            font-weight: normal;
            font-variant: normal;
            text-transform: none;
            line-height: 1;
            -webkit-font-smoothing: antialiased;
            width: 1em;
            display: inline-block;
            margin-right: 5px;
        }

        input[type="radio"]:checked + .label-text:before {
            content: "\f192";
            color: #fb6b04;
            animation: effect 250ms ease-in;
        }

        input[type="radio"]:disabled + .label-text {
            color: #aaa;
        }

        input[type="radio"]:disabled + .label-text:before {
            content: "\f111";
            color: #ccc;
        }

        /*Radio Toggle*/

        .toggle input[type="radio"] + .label-text:before {
            content: "\f204";
            font-family: "FontAwesome";
            speak: none;
            font-style: normal;
            font-weight: normal;
            font-variant: normal;
            text-transform: none;
            line-height: 1;
            -webkit-font-smoothing: antialiased;
            width: 1em;
            display: inline-block;
            margin-right: 10px;
        }

        .toggle input[type="radio"]:checked + .label-text:before {
            content: "\f205";
            color: #16a085;
            animation: effect 250ms ease-in;
        }

        .toggle input[type="radio"]:disabled + .label-text {
            color: #aaa;
        }

        .toggle input[type="radio"]:disabled + .label-text:before {
            content: "\f204";
            color: #ccc;
        }

        @keyframes effect {
            0% {
                transform: scale(0);
            }
            25% {
                transform: scale(1.3);
            }
            75% {
                transform: scale(1.4);
            }
            100% {
                transform: scale(1);
            }
        }

        #btn_submit {
            background: #04142a;
            color: white;
            font-size: 12px;
            margin-right: 15px;
            width: 200px;
        }

        span.pull-right {
            margin-top: -7px;
        }

        .col-xs-12.col-sm-4.col-md-4.col-lg-2.sidebar-holder {
            top: 75px;
        }

        .sidebar-menu, .main-sidebar .user-panel, .sidebar-menu > li.header {
            white-space: nowrap;
            margin-left: -15px;
            overflow: hidden;
            margin-right: -15px;
            font-family: "gotham-book";
            font-size: 18px;
        }

        .skin-blue .sidebar-menu > li:hover > a, .skin-blue .sidebar-menu > li.active > a {
            color: #fff;
            background: #0e3670;
            border: none;
        }

        .panel {
            border-radius: 0px;
        }

        .box-body {
            font-family: gotham-book;
        }

        .form-input {
            margin-left: 0px;
            width: 570px !important;
        }

        hr {
            margin-top: 20px;
            margin-bottom: 20px;
            border: 0;
            margin-left: -25px;

            border-top: 1px solid #04142a;
        }

        .this_button {
            background: #04142a !important;
            color: white;
            font-size: 12px;
            margin-right: 45px;
            width: 200px;
            font-family: gotham-book !important;
            border-radius: 5px;
        }

        .title-header {
            margin-left: 88px;
            font-weight: bold;
            font-family: gotham-medium;
            color: #04142a;
        }

        .btn-success {
            color: #fff;
            background-color: #04142a;
            border-color: #4cae4c;
        }


    </style>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@endsection
@section('content')
    <div class='row'>
        <div id="div_message">
        </div>
        <div id="app" class="panel">
            <!-- /.box-header -->
            <!-- form start -->
            <div class="box-body">
                <div class="col-sm-12">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                        <img src="/images/{{ Session::get('path') }}">
                    @endif
                </div>
                <h4 class="title-header">Add Credit Card</h4>
                <h4 class="title-header">Enter Your Credit Card Information</h4>
                <br>
                <form class="form-inline">

                    <div class="form-group">
                        <label for="" class="">Credit Card Number</label>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control form-input control-label" id="" placeholder="" value=""
                               style="width:393px !important; margin-left: 14px">
                    </div>
                    <br>
                    <br>

                    <div class="form-group">
                        <label for="" class="">Expiration Date</label>
                    </div>
                    <div class="form-group">
                        <select class="form-control form-input control-label" id="" placeholder=""
                                style="width: 200px !important;     margin-left: 45px;">
                            <option value="" selected> Month</option>
                        </select>
                        <select class="form-control form-input control-label" id="" placeholder=""
                                style="width: 188px !important;">
                            <option value="" selected> Year</option>
                        </select>
                    </div>
                    <br>
                    <br>
                    <div class="form-group">
                        <label for="" class="">Cardholder's Name</label>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control form-input control-label" id="" placeholder="" value=""
                               style="width:393px !important; margin-left: 19px">
                    </div>
                    <br>
                    <br>
                    <div class="form-group">
                        <label for="" class="">Credit Card Security</label>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control form-input control-label" id="" placeholder="" value=""
                               style="width:361px !important; margin-left: 11px">
                        <img style="width:25px;" src="{{ asset('images/WEB/questionmark.png') }}">
                    </div>
                    <br>
                    <br>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 col-md-3">
                                <img class="img-avatar" src="{{ asset("images/WEB/visa.png") }}">
                            </div>
                            <div class="col-sm-12 col-md-3">
                                <img class="img-avatar" src="{{ asset("images/WEB/mastercard.png") }}">
                            </div>
                            <div class="col-sm-12 col-md-3">
                                <img class="img-avatar" src="{{ asset("images/WEB/amex.png") }}">
                            </div>
                            <div class="col-sm-12 col-md-3">
                                <img class="img-avatar" src="{{ asset("images/WEB/paypal.png") }}">
                            </div>

                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="form-group">
                        <label for="" class="">Address</label>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control form-input control-label" id="" placeholder="" value=""
                               style="width:393px !important; margin-left: 94px">

                    </div>
                    <br>
                    <br>
                    <div class="form-group">
                        <label for="" class="">City</label>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control form-input control-label" id="" placeholder="" value=""
                               style="width:161px !important; margin-left: 123px">
                        <select  class="form-control form-input control-label"  style="width:226px !important;">
                            <option value="">Select State</option>
                        </select>
                    </div>
                    <br>
                    <br>
                    <div class="form-group">
                        <label for="" class="">Zip Code</label>
                        <div class="form-group">
                            <input type="text" class="form-control form-input control-label" id="" placeholder=""
                                   value="" style="width:100px !important; margin-left: 87px">
                            <label for="" class="">Country</label>
                            <select  class="form-control form-input control-label"  style="width:226px !important;">
                                <option value="">Country</option>
                            </select>

                        </div>
                    </div>
                    <br><br><br>
                    <div class="form-group">
                        <label for="" class="">This credit card will be set as the default payment method.<br>
                            We will use your Default Payment Method to automatically top up your<br>wallet for the
                            following circumstances:<br>
                            <br>
                            - Your wallet does not contain enough funds to complete your order.<br>
                            - Your wallet setting "Automatically Add Funds to Renew Subscriptions<br>or Pay for Pre-Ordered
                            Content" is to set enable* and we are attempting<br>to charge your wallet for a subscription
                            renewal <br>or pre-order and your wallet does not contain enough funds to cover the cost.
                            <br>
                            <br>
                            * We automatically enable this setting any time you buy a subscription<br>or make a
                            pre-order.<br>
                            You can manage this setting in your wallet.</label>
                    </div>
                    <br>
                    <br>
                    <br>
                    <div class="form-group  form-input" style="width:590px !important;">
                        <button type="submit" class="btn btn-info flat-button pull-right this_button"
                                data-loading-text="Please wait...">Add Payment</button>
                    </div>
                </form>
                <!-- /.box-body -->

                <!-- /.box-footer -->

            </div>

            <!-- /.box -->

            @endsection

            @section('footer_scripts')
                <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

                <!-- ClubNine -->
                {{--    <script src="{{ config('crm.url') . '/ClubNine-1.0.0/js/countries.js' }}"></script>--}}

                <script type="text/javascript">
                    $(document).ready(function () {
                        $('#btn-change-avatar').click(function () {
                            $('#open-file-image').click();
                        });

                        $('#btn-save').click(function () {
                            var profile_data = new FormData($('form#form-update-avatar')[0]);



                            {{--$.ajax({--}}
                            {{--type: 'POST',--}}
                            {{--url:" /account/profile/update/{{$profile['id']}}",--}}
                            {{--data:avatar_data,--}}
                            {{--dataType: 'json',--}}
                            {{--success:function(data) {--}}
                            {{--alert("Profile Successfully Updated!");--}}
                            {{--},--}}
                            {{--});--}}


                        });
                    });


                    function changeImage(input) {
                        var url = input.value;
                        var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();

                        if (input.files && input.files[0] && (ext == "png" || ext == "jpeg" || ext == "jpg")) {
                            if ((input.files[0].size / 1024) <= 2000) {

                                // console.log(input.files[0]);

                                var reader = new FileReader();
                                reader.onload = function (e) {
                                    $('#avatar').attr('src', e.target.result);
                                };
                                reader.readAsDataURL(input.files[0]);
                                var avatar_data = new FormData($('form#update-avatar')[0]);
                                swal(
                                    'Success!',
                                    'Please click save changes to save your avatar.',
                                    'success'
                                )
                            } else {
                                swal(
                                    'Error!',
                                    'Maximum image size is 2MB.',
                                    'error'
                                )

                            }

                        } else {
                            swal(
                                'Error!',
                                'Invalid image format.',
                                'error'
                            )
                        }

                    }

                    $('img').error(function () {

                        {{--@if(isset($profile_image_file))--}}
                        {{--$(".profile-image").attr("src", "{{ $profile_image_file }}");--}}
                        {{--@else--}}
                        {{--$(this).attr('src', "{{ asset('images/default_avatar.png') }}");--}}
                        {{--@endif--}}


                    });
                    @if(isset($profile_image_file))

                    $(".profile-image").attr("src", "{{$profile_image_file}}");
                    @else
                    $(this).attr('src', "{{ asset('images/default_avatar.png') }}");
                    @endif



                    $('#birth_date').attr('min', '1900-01-01');


                </script>


                <script>

                    // var select = document.getElementById("country");
                    // // Optional: Clear all existing options first:
                    // select.innerHTML = "";
                    // // Populate list with options:
                    // for (var i = 0; i < countries.length; i++) {
                    //     var opt = countries[i];
                    //     select.innerHTML += "<option value=\"" + opt.id + "\">" + opt.text + "</option>";
                    // }

                            {{--        var selectedcountry = "{{$profile['country']}}";--}}
                    var selectedcountry = "Philippines";
                    $('#country').val(selectedcountry.trim());

                    @isset($profile['gender'])
                    $('#gender').val("{{$profile['gender']}}");
                            @endisset


                    var dtToday = new Date();
                    var month = dtToday.getMonth() + 1;// jan=0; feb=1 .......
                    var day = dtToday.getDate();
                    var year = dtToday.getFullYear() - 18;
                    if (month < 10)
                        month = '0' + month.toString();
                    if (day < 10)
                        day = '0' + day.toString();
                    var maxDate = year + '-' + month + '-' + day;
                    var dateToday = (year + 18) + '-' + month + '-' + day;
                    $('#birth_date').attr('max', maxDate);

                    function validate_birthdate(e, dateToday) {
                        e.target.setCustomValidity('');
                        if ($('#birth_date').val() > dateToday || ($('#birth_date').val() <= $('#birth_date').attr('min') && $('#birth_date').val() != "")) {
                            e.target.setCustomValidity("Invalid date!");
                        }
                        else if ($('#birth_date').val() > maxDate && $('#birth_date').val() != "") {
                            e.target.setCustomValidity("You must be 18 years old and above!");
                            return;
                        }
                    }

                    $('#birth_date').on('focusout', function (e) {
                        validate_birthdate(e, dateToday);
                    });
                    $('#birth_date').on('change', function (e) {
                        validate_birthdate(e, dateToday);
                    });


                    $('#form_profile').on('submit', function (e) {
                        e.preventDefault();
                        var profile_data = new FormData(this);

                        {{--$.ajax({--}}
                        {{--type: 'POST',--}}
                        {{--url:" /account/profile/update/{{$profile['id']}}",--}}
                        {{--data:avatar_data,--}}
                        {{--dataType: 'json',--}}
                        {{--success:function(data) {--}}
                        {{--alert("Profile Successfully Updated!");--}}
                        {{--},--}}
                        {{--});--}}

                        // var formData = $('#form_profile');
                        var btn = $(this).find(':submit').button('loading');
                        $.ajax({
                            url: "/account/updateprofile",
                            type: "post",
                            // dataType: 'application/json',
                            data: profile_data,
                            // cache: false,
                            contentType: false,
                            processData: false,
                            success: function (data) {

                                $('#div_message').html('<div class="alert alert-warning alert-dismissible">\n' +
                                    '                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>\n' +
                                    data +
                                    '            </div>');
                                btn.button('reset');
                                console.log(data);

                            },
                            error: function (error) {
                                console.log(error);
                                btn.button('reset');

                            }

                        });
                    });
                </script>
@endsection