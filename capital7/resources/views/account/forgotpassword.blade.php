<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>NinePine Platform</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <style type="text/css">

        div#forgot-password h1 a {
            box-shadow: none!important;
            height: 170px;
            width: 100% !important;
            background-image: url({{asset("images/NINEPINE-LOGO.png")}}) !important;
            background-repeat: no-repeat;
            -webkit-background-size: 200px 170px;
            background-size: 200px 170px;
            background-position: center top;
            background-repeat: no-repeat;
            color: #999;
            /* height: 150px; */
            font-size: 20px;
            font-weight: 400;
            line-height: 1.3em;
            margin: 0 auto 25px;
            padding: 0;
            text-decoration: none;
            width: 80px;
            text-indent: -9999px;
            outline: 0;
            overflow: hidden;
            display: block;


        }
        #forgot-password {

            padding: 0 0 0;
            margin: auto;
            float: none;

        }
        .message{
            background-color: #ff8357;
            border-color: #ff8357;
            font-weight: 500;
            color: #FFF;
            box-shadow: none;
            padding: 10px;
        }
        .btn{
            border-radius: 0px;
            width: 90%;
        }

        form {

            margin-left: 0;
            padding: 26px 26px 2px;
            font-weight: 400;
            overflow: hidden;
            background: #fff;
            -webkit-box-shadow: 0 0 10px rgba(20, 128, 94,92);
            box-shadow: 0 0 10px rgba(20, 128, 94,92);
        }

        hr{
            border-top: 1px dashed #8c8b8b;
            border-bottom: 1px dashed #fff;
        }
        input.btn.btn-primary {
            border-radius: 0px;
        }
        .form-control{
            border-radius: 0px;
        }
        .strike {
            display: block;
            text-align: center;
            overflow: hidden;
            white-space: nowrap;

            font-size: 12px;
        }

        .strike > span {
            position: relative;
            display: inline-block;
        }

        .strike > span:before,
        .strike > span:after {
            content: "";
            position: absolute;
            top: 50%;
            width: 9999px;
            /* Here is the modification */
            border-top: 1px dashed #8c8b8b;
            border-bottom: 1px dashed #fff;
        }

        .strike > span:before {
            right: 100%;

        }

        .strike > span:after {
            left: 100%;
            margin-left: 5px;
            margin-bottom: 5px;
        }
        .strike{
            margin-right: 15px;
        }
        .nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
            color: #fff;
            background-color: #14805e;
            border-radius: 0px;
        }
        .btn-success {
            color: #fff;
            background-color: #14805e;
            border-color: #14805e
        }
        .nav-pills>li>a {
            border-radius: 0px;
        }
    </style>
</head>
<body>
<div class="container-fluid">

    <div id="forgot-password" class="col-xs-12 col-sm-2">
        <h1>
            <a href="#" title="Ninepine Tech">Ninepine Tech</a>
        </h1>
        <hr>
        @if (Session::has('message'))
            <div id="message" class="message">{{ Session::get('message') }}</div>

            @else
            <div id="message" class="message">Please select password reset method below.</div>
        @endif

        <hr>
        <div class="form-group">
            <ul class="nav nav-tabs nav-pills">
                <li class="active"><a data-toggle="tab" href="#reset_email">Using email</a></li>
                <li><a id="btnAnswer" data-toggle="tab" href="#div_secret_question">Answer secret question</a></li>
            </ul>

            <div class="tab-content">
                <div id="reset_email" class="tab-pane fade in active">
                    <form name="lostpasswordform" id="lostpasswordform" action="/resetViaEmail" method="post">
                        {{csrf_field()}}
                        <div class="row">
                            <div id="message1" class="message" hidden></div>
                        </div>
                        <div class="form-group">
                            <label for="secret-question">If you can access your email address enter your email address below.</label>
                            <hr>
                            <label for="forgot-password-email-user"><i class="fa fa-envelope-o" aria-hidden="true"></i> EMAIL:</label>
                            <input type="email" name="email" id="email" class="form-control" required>
                        </div>
                        <p class="text-center">
                            <button id="btn_submit" type="submit"  data-loading-text='LOADING' class="btn btn-success" > <i class="fa fa-paper-plane-o" aria-hidden="true"></i>  Proceed</button></p>
                        </p>
                    </form>
                </div>
                <div id="div_secret_question" class="tab-pane fade">
                    <div id="div_reset_password_security_question">
                        <form name="form-secret-question" id="form-secret-question"  method="post" action="/security_question_fetch">
                            <div class="row">
                                @if (Session::has('error'))
                                    <div id="message" class="message2 alert alert-danger">{{ Session::get('error') }}</div>
                                @endif
                            </div>

                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="secret-question">Reset password via answering secret question.</label>
                                <hr>
                                <label for="secret-question">Enter your username/email first: </label>
                                <input type="text" name="email_user" id="email_user" class="form-control" placeholder="Email/Username" required>
                            </div>
                            <p class="text-center">
                                <button type="submit" id="btn_submit_security_1" class="btn btn-success" data-loading-text='LOADING'> <i class="fa fa-paper-plane-o" aria-hidden="true"></i> Proceed</button>
                            </p>

                        </form>

                    </div>




                </div>
            </div>
        </div>


        <hr>

    </div>

</div>
<script src="https://code.jquery.com/jquery-2.x-git.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script>

    $('#lostpasswordform').submit(function(e){

        var form = $(this);
        var btn = form.find(':submit').button('loading');
        e.preventDefault();
        var formData = $('#lostpasswordform').serialize();

        $.ajax({
            url: "/resetViaEmail",
            type: "post",
            data:formData,
            success: function(data) {
                $('#reset_email').html("<hr><div class=\"alert alert-success\">" + data + "</div>");
                btn.button('reset');
            },
            error: function(error){  btn.button('reset');
                btn.button('reset');
                $('#message1').html("Invalid email/username.");
                $('#message1').show();;
            }


        });

    });







    function isValidEmailAddress(emailAddress) {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        return pattern.test(emailAddress);
    }


</script>
<script type="text/javascript">
    $(document).ready(function () {
        //Disable full page
        $("body").on("contextmenu",function(e){
            return false;
        });

        //Disable part of page
        $("#id").on("contextmenu",function(e){
            return false;
        });
    });
</script>
@if (Session::has('error'))
    <script>$('#btnAnswer').click();</script>
@endif
</body>
</html>