@extends('layouts.default')
@section('header_styles')
    <style>


        .devices-container {
            padding-right: 0;
            padding-left: 0;
        }
        .device-container {
            padding-right: 7px;
            padding-left: 7px;
        }

        .device-header {
            font-weight: bold;
            font-size: 110%;
        }
        .phone-img-holder{
            background-color: white;
        }
        .info-box-icon {
            background: white;
        }
        .box.box-solid>.box-header {
            color: #fff;
            background: #14805e;
            background-color: #14805e;
            border-color: #14805e;
        }


        .box.box-solid.box-primary {
            border: 1px solid #14805e;
        }

    </style>
@endsection
@section('content')
    <div class='row'>
        <div class='col-md-12'>
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Devices</h3>
                </div>
                <div class="box-body" id="device_body">

                    <div class="col-md-12 devices-container">

                        <div class="col-md-4">
                            <div class="box box-solid  box-primary app-box ">
                                <div class="box-body">
                                    <div class="info-box">
                                        <!-- Apply any bg-* class to to the icon to color it -->
                                        <span class="info-box-icon"><img src="{{ url('/images/phone.png') }}" height="90" width="90" class="phone-img-holder"></span>
                                        <div class="info-box-content">

                                            <p class="device-header">Samsung S8</p>
                                            <div><strong>Model Number:</strong> SMG950</div>
                                            <div><strong>OS:</strong> Android 7.0</div>
                                            <div><strong>Name:</strong> Nougat</div>

                                        </div>

                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->

                                    <span class="pull-right">
                  <button class="btn btn-danger"> <i class="fa fa-trash-o" aria-hidden="true"></i> Remove </button>

                </span>

                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="box box-solid  box-primary app-box ">
                                <div class="box-body">
                                    <div class="info-box">
                                        <!-- Apply any bg-* class to to the icon to color it -->
                                        <span class="info-box-icon"><img src="{{ url('/images/phone.png') }}" height="90" width="90" class="phone-img-holder"></span>
                                        <div class="info-box-content">

                                            <p class="device-header">Samsung S5</p>
                                            <div><strong>Model Number:</strong> SM-GACOF</div>
                                            <div><strong>OS:</strong> Android 6.0.1</div>
                                            <div><strong>Name:</strong> Marshmallow</div>

                                        </div>

                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->

                                    <span class="pull-right">
                  <button class="btn btn-danger"> <i class="fa fa-trash-o" aria-hidden="true"></i> Remove </button>

                </span>

                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="box box-solid  box-primary app-box ">
                                <div class="box-body">
                                    <div class="info-box">
                                        <!-- Apply any bg-* class to to the icon to color it -->
                                        <span class="info-box-icon"><img src="{{ url('/images/phone.png') }}" height="90" width="90" class="phone-img-holder"></span>
                                        <div class="info-box-content">

                                            <p class="device-header">Samsung Tablet</p>
                                            <div><strong>Model Number:</strong> SM-T535</div>
                                            <div><strong>OS:</strong> Android 4.4.2</div>
                                            <div><strong>Name:</strong> KitKat</div>

                                        </div>

                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->

                                    <span class="pull-right">
                  <button class="btn btn-danger"> <i class="fa fa-trash-o" aria-hidden="true"></i> Remove </button>

                </span>

                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>

                    </div>


                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.box -->

    </div>



@endsection

@section('footer_scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            //
        });
    </script>
@endsection
