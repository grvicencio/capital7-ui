@extends('layouts.default')
@section('header_styles')
    <style>
        .flat-button {
            border-radius: 0px;
            height: 35px;
        }

        .img-thumbnail {
            width: 150px;
            height: 150px;
        }

        html, body {
            overflow-y: auto !important;
            overflow-x: hidden !important;
        }

        div#app {
            height: -webkit-fill-available;
        }

        input[type="checkbox"], input[type="radio"] {
            position: absolute;
            right: 9000px;
        }

        /*Check box*/
        input[type="checkbox"] + .label-text:before {
            content: "\f096";
            font-family: "FontAwesome";
            speak: none;
            font-style: normal;
            font-weight: normal;
            font-variant: normal;
            text-transform: none;
            line-height: 1;
            -webkit-font-smoothing: antialiased;
            width: 1em;
            display: inline-block;
            margin-right: 5px;
        }

        input[type="checkbox"]:checked + .label-text:before {
            content: "\f14a";
            color: #2980b9;
            animation: effect 250ms ease-in;
        }

        input[type="checkbox"]:disabled + .label-text {
            color: #aaa;
        }

        input[type="checkbox"]:disabled + .label-text:before {
            content: "\f0c8";
            color: #ccc;
        }

        /*Radio box*/

        input[type="radio"] + .label-text:before {
            content: "\f10c";
            font-family: "FontAwesome";
            speak: none;
            font-style: normal;
            font-weight: normal;
            font-variant: normal;
            text-transform: none;
            line-height: 1;
            -webkit-font-smoothing: antialiased;
            width: 1em;
            display: inline-block;
            margin-right: 5px;
        }

        input[type="radio"]:checked + .label-text:before {
            content: "\f192";
            color: #fb6b04;
            animation: effect 250ms ease-in;
        }

        input[type="radio"]:disabled + .label-text {
            color: #aaa;
        }

        input[type="radio"]:disabled + .label-text:before {
            content: "\f111";
            color: #ccc;
        }

        /*Radio Toggle*/

        .toggle input[type="radio"] + .label-text:before {
            content: "\f204";
            font-family: "FontAwesome";
            speak: none;
            font-style: normal;
            font-weight: normal;
            font-variant: normal;
            text-transform: none;
            line-height: 1;
            -webkit-font-smoothing: antialiased;
            width: 1em;
            display: inline-block;
            margin-right: 10px;
        }

        .toggle input[type="radio"]:checked + .label-text:before {
            content: "\f205";
            color: #16a085;
            animation: effect 250ms ease-in;
        }

        .toggle input[type="radio"]:disabled + .label-text {
            color: #aaa;
        }

        .toggle input[type="radio"]:disabled + .label-text:before {
            content: "\f204";
            color: #ccc;
        }

        @keyframes effect {
            0% {
                transform: scale(0);
            }
            25% {
                transform: scale(1.3);
            }
            75% {
                transform: scale(1.4);
            }
            100% {
                transform: scale(1);
            }
        }

        #btn_submit {
            background: #04142a;
            color: white;
            font-size: 12px;
            margin-right: 15px;
            width: 200px;
        }

        span.pull-right {
            margin-top: -7px;
        }

        .col-xs-12.col-sm-4.col-md-4.col-lg-2.sidebar-holder {
            top: 75px;
        }

        .sidebar-menu, .main-sidebar .user-panel, .sidebar-menu>li.header {
            white-space: nowrap;
            margin-left: -15px;
            overflow: hidden;
            margin-right: -15px;
            font-family: "gotham-book";
            font-size: 18px;
        }

        .skin-blue .sidebar-menu>li:hover>a, .skin-blue .sidebar-menu>li.active>a {
            color: #fff;
            background: #0e3670;
            border: none;
        }
        .panel{
            border-radius: 0px;
        }
        .box-body{
            font-family: gotham-book;
        }


        .form-input {
            margin-left: 0px;
            width: 570px !important;
        }

        .checkbox input[type="checkbox"] {
            opacity: 0;
        }

        .checkbox label {
            position: relative;
            display: inline-block;

            /*16px width of fake checkbox + 6px distance between fake checkbox and text*/
            padding-left: 22px;
        }

        .checkbox label::before,
        .checkbox label::after {
            position: absolute;
            content: "";

            /*Needed for the line-height to take effect*/
            display: inline-block;
        }

        /*Outer box of the fake checkbox*/
        .checkbox label::before{
            height: 16px;
            width: 16px;

            border: 1px solid;
            left: 0px;

            /*(24px line-height - 16px height of fake checkbox) / 2 - 1px for the border
             *to vertically center it.
             */
            top: 3px;
        }

        /*Checkmark of the fake checkbox*/
        .checkbox label::after {
            height: 5px;
            width: 9px;
            border-left: 2px solid;
            border-bottom: 2px solid;

            transform: rotate(-45deg);

            left: 4px;
            top: 7px;
        }

        /*Hide the checkmark by default*/
        .checkbox input[type="checkbox"] + label::after {
            content: none;
        }

        /*Unhide on the checked state*/
        .checkbox input[type="checkbox"]:checked + label::after {
            content: "";
        }

        /*Adding focus styles on the outer-box of the fake checkbox*/
        .checkbox input[type="checkbox"]:focus + label::before {
            outline: rgb(59, 153, 252) auto 5px;
        }

        .form-inline .checkbox label, .form-inline .radio label {
            padding-left: 30px;
        }


        .cbx {
            margin: auto;
            -webkit-user-select: none;
            user-select: none;
            cursor: pointer;
        }
        .cbx span {
            display: inline-block;
            vertical-align: middle;
            transform: translate3d(0, 0, 0);
        }
        .cbx span:first-child {
            position: relative;
            width: 18px;
            height: 18px;
            border-radius: 3px;
            transform: scale(1);
            vertical-align: middle;
            border: 1px solid #9098A9;
            transition: all 0.2s ease;
        }
        .cbx span:first-child svg {
            position: absolute;
            top: 3px;
            left: 2px;
            fill: none;
            stroke: #FFFFFF;
            stroke-width: 2;
            stroke-linecap: round;
            stroke-linejoin: round;
            stroke-dasharray: 16px;
            stroke-dashoffset: 16px;
            transition: all 0.3s ease;
            transition-delay: 0.1s;
            transform: translate3d(0, 0, 0);
        }
        .cbx span:first-child:before {
            content: "";
            width: 100%;
            height: 100%;
            background: #506EEC;
            display: block;
            transform: scale(0);
            opacity: 1;
            border-radius: 50%;
        }
        .cbx span:last-child {
            padding-left: 8px;
        }
        .cbx:hover span:first-child {
            border-color: #506EEC;
        }

        .inp-cbx:checked + .cbx span:first-child {
            background: #506EEC;
            border-color: #506EEC;
            animation: wave 0.4s ease;
        }
        .inp-cbx:checked + .cbx span:first-child svg {
            stroke-dashoffset: 0;
        }
        .inp-cbx:checked + .cbx span:first-child:before {
            transform: scale(3.5);
            opacity: 0;
            transition: all 0.6s ease;
        }

        @keyframes wave {
            50% {
                transform: scale(0.9);
            }
        }

        .cbx span:last-child {
            padding-left: 0px;
            margin-left: 25px;
        }

        .flat-button {
            border-radius: 0px;
            height: 35px;
            color: #fff;
            background-color: #04142a;
            /* border-color: #4cae4c; */
        }

    </style>
@endsection
@section('content')
    <div class='row'>
        <div id="div_message">
        </div>
        <div id="app" class="panel">
            <!-- /.box-header -->
            <!-- form start -->
            <div class="box-body">
                <div class="col-sm-12">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                        </div>
                        <img src="/images/{{ Session::get('path') }}">
                    @endif
                </div>

                <h3 class="title-header">Notification Preferences</h3>
                <br>
                <br>
                <form class="form-inline">
                    <div class="form-group">

                        <input class="inp-cbx" id="cbx" type="checkbox" style="display: none;"/>
                        <label class="cbx" for="cbx"><span>
                        <svg width="12px" height="10px" viewbox="0 0 12 10">
                          <polyline points="1.5 6 4.5 9 10.5 1"></polyline>
                        </svg></span><span>Yes, I am interested in receiving marketing materials from PINES<br>
(SIE). I understand I can unsubscribe at any time.</span></label>

                    </div>
                    <br>
                    <br>
                        <div class="form-group">

                        <input class="inp-cbx" id="cbx" type="checkbox" style="display: none;"/>
                        <label class="cbx" for="cbx"><span>
                        <svg width="12px" height="10px" viewbox="0 0 12 10">
                          <polyline points="1.5 6 4.5 9 10.5 1"></polyline>
                        </svg></span><span>Yes, I agree to allow PINES to share my personal information with<br>
                        PINES’s partners for marketing purposes. I understand I can unsubscribe at any time.</span></label>

                    </div>
                    <br>
                    <br>
                    <div class="form-group">
                        <label for="exampleInputName2">For more information about our privacy practices, click the “Privacy Policy” link on this page.</label>

                    </div>

                    <br>
                    <br>

                    <div class="form-group  form-input" style="width:590px !important;">
                        <button  type="submit" class="btn btn-info flat-button pull-left this_button"
                                 data-loading-text="Please wait...">Update Settings
                        </button>
                    </div>
                </form>
                <!-- /.box-body -->

                <!-- /.box-footer -->

            </div>

            <!-- /.box -->

        @endsection

        @section('footer_scripts')

            <!-- ClubNine -->
                {{--    <script src="{{ config('crm.url') . '/ClubNine-1.0.0/js/countries.js' }}"></script>--}}

                <script type="text/javascript">
                    $(document).ready(function () {
                        $('#btn-change-avatar').click(function () {
                            $('#open-file-image').click();
                        });

                        $('#btn-save').click(function () {
                            var profile_data = new FormData($('form#form-update-avatar')[0]);



                            {{--$.ajax({--}}
                            {{--type: 'POST',--}}
                            {{--url:" /account/profile/update/{{$profile['id']}}",--}}
                            {{--data:avatar_data,--}}
                            {{--dataType: 'json',--}}
                            {{--success:function(data) {--}}
                            {{--alert("Profile Successfully Updated!");--}}
                            {{--},--}}
                            {{--});--}}


                        });
                    });


                    function changeImage(input) {
                        var url = input.value;
                        var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();

                        if (input.files && input.files[0] && (ext == "png" || ext == "jpeg" || ext == "jpg")) {
                            if ((input.files[0].size / 1024) <= 2000) {

                                // console.log(input.files[0]);

                                var reader = new FileReader();
                                reader.onload = function (e) {
                                    $('#avatar').attr('src', e.target.result);
                                };
                                reader.readAsDataURL(input.files[0]);
                                var avatar_data = new FormData($('form#update-avatar')[0]);
                                swal(
                                    'Success!',
                                    'Please click save changes to save your avatar.',
                                    'success'
                                )
                            } else {
                                swal(
                                    'Error!',
                                    'Maximum image size is 2MB.',
                                    'error'
                                )

                            }

                        } else {
                            swal(
                                'Error!',
                                'Invalid image format.',
                                'error'
                            )
                        }

                    }

                    $('img').error(function () {

                        {{--@if(isset($profile_image_file))--}}
                        {{--$(".profile-image").attr("src", "{{ $profile_image_file }}");--}}
                        {{--@else--}}
                        {{--$(this).attr('src', "{{ asset('images/default_avatar.png') }}");--}}
                        {{--@endif--}}


                    });
                    @if(isset($profile_image_file))

                    $(".profile-image").attr("src", "{{$profile_image_file}}");
                    @else
                    $(this).attr('src', "{{ asset('images/default_avatar.png') }}");
                    @endif



                    $('#birth_date').attr('min', '1900-01-01');


                </script>


                <script>

                    // var select = document.getElementById("country");
                    // // Optional: Clear all existing options first:
                    // select.innerHTML = "";
                    // // Populate list with options:
                    // for (var i = 0; i < countries.length; i++) {
                    //     var opt = countries[i];
                    //     select.innerHTML += "<option value=\"" + opt.id + "\">" + opt.text + "</option>";
                    // }

                            {{--        var selectedcountry = "{{$profile['country']}}";--}}
                    var selectedcountry = "Philippines";
                    $('#country').val(selectedcountry.trim());

                    @isset($profile['gender'])
                    $('#gender').val("{{$profile['gender']}}");
                            @endisset


                    var dtToday = new Date();
                    var month = dtToday.getMonth() + 1;// jan=0; feb=1 .......
                    var day = dtToday.getDate();
                    var year = dtToday.getFullYear() - 18;
                    if (month < 10)
                        month = '0' + month.toString();
                    if (day < 10)
                        day = '0' + day.toString();
                    var maxDate = year + '-' + month + '-' + day;
                    var dateToday = (year + 18) + '-' + month + '-' + day;
                    $('#birth_date').attr('max', maxDate);

                    function validate_birthdate(e, dateToday) {
                        e.target.setCustomValidity('');
                        if ($('#birth_date').val() > dateToday || ($('#birth_date').val() <= $('#birth_date').attr('min') && $('#birth_date').val() != "")) {
                            e.target.setCustomValidity("Invalid date!");
                        }
                        else if ($('#birth_date').val() > maxDate && $('#birth_date').val() != "") {
                            e.target.setCustomValidity("You must be 18 years old and above!");
                            return;
                        }
                    }

                    $('#birth_date').on('focusout', function (e) {
                        validate_birthdate(e, dateToday);
                    });
                    $('#birth_date').on('change', function (e) {
                        validate_birthdate(e, dateToday);
                    });


                    $('#form_profile').on('submit', function (e) {
                        e.preventDefault();
                        var profile_data = new FormData(this);

                        {{--$.ajax({--}}
                        {{--type: 'POST',--}}
                        {{--url:" /account/profile/update/{{$profile['id']}}",--}}
                        {{--data:avatar_data,--}}
                        {{--dataType: 'json',--}}
                        {{--success:function(data) {--}}
                        {{--alert("Profile Successfully Updated!");--}}
                        {{--},--}}
                        {{--});--}}

                        // var formData = $('#form_profile');
                        var btn = $(this).find(':submit').button('loading');
                        $.ajax({
                            url: "/account/updateprofile",
                            type: "post",
                            // dataType: 'application/json',
                            data: profile_data,
                            // cache: false,
                            contentType: false,
                            processData: false,
                            success: function (data) {

                                $('#div_message').html('<div class="alert alert-warning alert-dismissible">\n' +
                                    '                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>\n' +
                                    data +
                                    '            </div>');
                                btn.button('reset');
                                console.log(data);

                            },
                            error: function (error) {
                                console.log(error);
                                btn.button('reset');

                            }

                        });
                    });
                </script>
@endsection