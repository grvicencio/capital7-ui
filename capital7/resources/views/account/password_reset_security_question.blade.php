<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>NinePine Platform</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <style type="text/css">

        div#forgot-password h1 a {
            box-shadow: none!important;
            height: 170px;
            width: 100% !important;
            background-image: url({{asset("images/NINEPINE-LOGO.png")}}) !important;
            background-repeat: no-repeat;
            -webkit-background-size: 200px 170px;
            background-size: 200px 170px;
            background-position: center top;
            background-repeat: no-repeat;
            color: #999;
            /* height: 150px; */
            font-size: 20px;
            font-weight: 400;
            line-height: 1.3em;
            margin: 0 auto 25px;
            padding: 0;
            text-decoration: none;
            width: 80px;
            text-indent: -9999px;
            outline: 0;
            overflow: hidden;
            display: block;


        }
        #forgot-password {

            padding: 0 0 0;
            margin: auto;
            float: none;

        }
        .message{
            background-color: #fd875c;
            border-color: #fd875c;
            font-weight: 500;
            color: #FFF;
            box-shadow: none;
            padding: 10px;
        }
        .btn{
            border-radius: 0px;
            width: 80%;
        }

        form {
            margin-top: 20px;
            margin-left: 0;
            padding: 26px 26px 2px;
            font-weight: 400;
            overflow: hidden;
            background: #fff;
            -webkit-box-shadow: 0 0 10px rgba(0,0,0,.13);
            box-shadow: 0 0 10px rgba(0,0,0,.13);
        }

        hr{
            border-top: 1px dashed #8c8b8b;
            border-bottom: 1px dashed #fff;
        }
        input.btn.btn-primary {
            border-radius: 0px;
        }
        .form-control{
            border-radius: 0px;
        }
        .strike {
            display: block;
            text-align: center;
            overflow: hidden;
            white-space: nowrap;

            font-size: 12px;
        }

        .strike > span {
            position: relative;
            display: inline-block;
        }

        .strike > span:before,
        .strike > span:after {
            content: "";
            position: absolute;
            top: 50%;
            width: 9999px;
            /* Here is the modification */
            border-top: 1px dashed #8c8b8b;
            border-bottom: 1px dashed #fff;
        }

        .strike > span:before {
            right: 100%;

        }

        .strike > span:after {
            left: 100%;
            margin-left: 5px;
            margin-bottom: 5px;
        }
        .strike{
            margin-right: 15px;
        }
        div.alert.alert-danger {
            text-overflow: ellipsis;
            word-wrap: break-word;
        }
    </style>
</head>
<body>
<div class="container-fluid">
    <div id="forgot-password" class="col-xs-12 col-sm-2">
        <h1>
            <a href="#" title="Ninepine Tech">Ninepine Tech</a>
        </h1>
        <hr>
        <div id="div_secret_question">
            <p class="message">Please enter your answer below.</p>
            <form id="form_reset_password_secret_question_step1" name="form_reset_password_secret_question_step1" action="" method="post">
                <div class="row">
                    <div id="message" class="alert alert-danger message2" hidden></div>
                </div>
                {{csrf_field()}}
                <div class="form-group">
                    <input type="hidden" name="email_user_security_password" required class="form-control" placeholder="Answer" value="{{$email}}">
                    <label for="secret-question" id="secret-question">{{$security_question}}</label>
                    <input type="text" name="answer" id="answer" required class="form-control" placeholder="Answer">
                </div>
                <p class="text-center">
                    <button type="submit" class="btn btn-success" data-loading-text='LOADING'> <i class="fa fa-paper-plane-o" aria-hidden="true"></i> Continue</button>
                </p>
            </form>

        </div>
        <div id="div_secret_question_step2" hidden>
            <form id="form_reset_password_secret_question_step2" name="form_reset_password_secret_question_step2" action="" method="post">

            </form>
        </div>



        <hr>
    </div>
</div>
<script src="https://code.jquery.com/jquery-2.x-git.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script>
    $('#form_reset_password_secret_question_step1').submit(function (e){
        var form = $(this);
        var btn = form.find(':submit').button('loading');
        e.preventDefault();
        var formData = $('#form_reset_password_secret_question_step1').serialize();
        $.ajax({
            url: "/checkSecurityQuestionAnswer",
            type: "post",
            data:formData,
            success: function(data) {
                data = (jQuery.parseJSON(data));
                btn.button('reset');
                if(data.result == 'failed'){
                    $('#message').html(data.message);
                    $('#message').show();
                }
                else if(data.result == 'success'){
                    var answer = $('#answer').val();
                    $('#div_secret_question').html('');
                    $('#form_reset_password_secret_question_step2').html('<p id="message" class="message">Please enter your new password below.</p>\n' +
                        ' {{csrf_field()}}\n' +
                        '           \n' +
                        '<div class="form-group">\n' +
                        '                <input type="hidden" name="email_user_security_password" id="email_user_security_password" required class="form-control" placeholder="Answer" value="{{$email}}">\n' +
                        '                <input type="hidden" id="answer" name="answer" required class="form-control" placeholder="Answer">\n' +
                        '                <label for="secret-question" id="secret-question">New Password</label>\n' +
                        '                <input type="password" name="password" id="password" required class="form-control" placeholder="Password">\n' +
                        '                <label for="secret-question" id="secret-question">Repeat Password</label>\n' +
                        '                <input type="password" name="password_confirmation" id="password_confirmation" required class="form-control" placeholder="Repeat Password">\n' +
                        '</div> \n' +
                       ' <p class="text-center"> \n' +
                        '<button type="submit" class="btn btn-success" data-loading-text="LOADING"> <i class="fa fa-paper-plane-o" aria-hidden="true"></i> Continue</button> \n' +
                      '</p>');

                    $('#answer').val(answer);
                    $('#div_secret_question_step2').show();

                }

            },
            error: function(xhr,status, response){
                btn.button('reset');
                var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
                var info = $('#message');
                info.html('');
                for(var k in error.message){
                    if(error.message.hasOwnProperty(k)){
                        error.message[k].forEach(function(val){
                            info.append('<li>' + val + '</li>');
                        });
                    }
                }
                $('#message').show();
            }
        });




    });
    $('#form_reset_password_secret_question_step2').submit(function (e){
        var form = $(this);
        var btn = form.find(':submit').button('loading');
        e.preventDefault();
        var formData = $('#form_reset_password_secret_question_step2').serialize();
        $.ajax({
            url: "/reset_password_security_question",
            type: "post",
            data:formData,
            success: function(data) {
                data = (jQuery.parseJSON(data));
                btn.button('reset');
                if(data.result == 'failed'){
                    $('#message').html(data.message);
                    $('#message').show();
                }
                else if(data.result == 'success'){

                    $('#div_secret_question_step2').html( '<p class="message">'+ data.message +' Please click <a href="/">here</a> to login.</p>')

                }

            },
            error: function(xhr,status, response){
                btn.button('reset');
                var error = jQuery.parseJSON(xhr.responseText);  // this section is key player in getting the value of the errors from controller.
                var info = $('#message');
                info.html('');
                for(var k in error.message){
                    if(error.message.hasOwnProperty(k)){
                        error.message[k].forEach(function(val){
                            info.append('<li>' + val + '</li>');
                        });
                    }
                }
                $('#message').show();
            }
        });



    });
</script>

</body>


</html>